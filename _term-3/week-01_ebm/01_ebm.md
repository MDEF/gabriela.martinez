---
title: 01 • Emergent Business Models
period: 16-30 May 2019
date: 2019-05-30 12:00:00
term: 3
published: true
---


<br>
<br>
*Emergent Business Models* was a course led by Javier Creus and Valeria Righi from [Ideas for Change](https://www.ideasforchange.com/). We were introduced to concepts around systems and how to create an impact that later replicate the innovative model. The core concepts presented to us were around of how to maximize an impact, minimize efforts and maximize probability. Different solutions to create impact were discussed and also other topics related to it, as dissemination and funding. The impact of the technological changes were also commented, as the presence of sharing economy and the rise of AI in our systems.  
<br>
<br>
In the first session, we did an activity to define our impact variable, an analysis of previous innovations and a refinement our idea. The result of my reflections over this activity can be seen below.
<br>
<br>
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQQceVD9Aa7S1wNvywaVxMo01Rl0YUgOaT01D6LMHeRb0XiXcLw6-Kfo3xnai9TYJvoCidZrEIQZriY/embed?start=false&loop=false&delayms=3000" frameborder="0" width="680" height="424" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<br>
<br>
In the second session, more examples of enterprises and businesses that created an impact in their chosen fields were presented to us. Moreover, 5 key aspects to grow an initiative were introduced to us. Part of the Pentagrowth methodology, the aspects are:
<br>
• Connect in network
<br>
• Collect inventory
<br>
• Empower users
<br>
• Enable partners
<br>
• Share knowledge
<br>
<br>
The activity for this second session consisted in exploring elements in our systems that could be leveraged on. We would do that by completing a newspaper page from the future. We needed to project the changes we foresee and trace back the historical ones that are similar in their propositions. My definitions were:
<br>
*System*: Making / Producing in the city - Social Innovation / Grassroots movements
<br>
*What variable of the system do you want to change/impact?*: The disconnection of society from production and material world, leading to irresponsible consumption patterns, which are also unsustainable.
<br>
*Historical system changes*:
<br>
**Arts & Crafts Movement** (1880-1920): the movement advocated for economic and social reform, positioning itself with craftsmanship concepts. It brought an alternative that was more attuned to the cultural and material context of producing.
<br>
**Bauhaus** (1919-1933): the school focused on design and fine arts, aligning it to industrial production, revolutionizing both worlds. Craft tradition and modern technology were combined. The expressiveness of its concept is still relevant to current times.
<br>
**Maker’s Movement/ Fab Labs** (2002-present): Fab Labs are small scale workshops enabling digital fabrication. With the access to rapid prototyping tools, communities are empowered to create machines, systems, services or products. Grassroots movements and social innovation are concepts tied to this idea.
<br>
My proposition:
<br>
**Local Connection Through Making**: Makerspaces are a great network to start doing things, but most are build with standard machines and people are experiment with the same materials. The idea is to foster local narratives to be built with material and cultural consciousness.
<br>
*Minimizing efforts (product features/behaviour change)*: The idea is to build over the existing network of makerspaces, connecting them with important local crafts regions, adding material and cultural consciousness in the process of making.
<br>
*Leveraging on the system: On which elements of the system do you leverage on?* Communities: Local - neighbors, newcomers, marginalized people. Data: Local/popular/embodied knowledge. Assets: shared knowledge between craftspeople and technologists. Partners: makers, local makerspaces, fab labs, universities
<br>
<br>
On the third and last session, all students presented their newspapers and we could share our ideas and receive feedback.
<br>
<br>
Overall, the course was informative in showing us systems and actions to impact in certain fields, like mobility, housing or societies’ relations. It was a good exercise to track back interventions from the past to inspire our actions. For me this was very helpful in showing a structure to follow. Additionally, helped to build a sustainability model for my project, showing me potentialities in the education and production field.

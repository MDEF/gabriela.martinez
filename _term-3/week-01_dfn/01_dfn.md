---
title: 01 • Design for the New
period: 26 April - 17 May 2019
date: 2019-05-17 12:00:00
term: 3
published: true
---


<br>
<br>
“Design for the New” was a course led by Mercè Rua and Markel Cormenzana from [HOLON](http://www.holon.cat/en/). The goal was to develop a transitional view through the design of an everydayness practice. The first session was about approaching the practice, the second about designing the transitional pathway and the third about performing a scenario that would include the new practice. Practice, as defined by Reckwitz (2002a:249) is *“a routinized type of behaviour which consists of several elements, interconnected to one another: forms of bodily activities, forms of mental activities, ‘things’ and their use, a background knowledge in the form of understanding, know-how, states of emotion and motivational knowledge”*.
<br>
<br>
We were questioned about which are the everyday practices connected to our projects, how would we materialize our emergent futures and which would be the right path to get there. Then, we were introduced to the Transition Design framework, which brings a new knowledge and skill sets. From this process, we would focus on designing a practice for the future, by analysing the current practice and finding the transitional pathway to achieve the future one. To break down a practice, we would think about the skills, stuffs and images around it and later project the ones we foresee for the future.
<br>
<br>
{% figure caption: "*Analysis of a practice*" %}
![]({{site.baseurl}}/dfn_w01_01.jpg)
{% endfigure %}
<br>
<br>
 I defined as a practice the act of producing. And the desired future practice would be to produce locally. These choices were done since my project seeks to explore the hybrid space in between the Commons knowledge of craftsmanship and the new emerging technologies for making in the urban context. It looks to safeguard cultural heritage for the futures while decolonizing design by the empowerment of local narratives.
<br>
<br>
{% figure caption: "*Current practice*" %}
![]({{site.baseurl}}/dfn_w01_02.jpg)
{% endfigure %}
{% figure caption: "*Desired practice*" %}
![]({{site.baseurl}}/dfn_w01_03.jpg)
{% endfigure %}
<br>
<br>
For the transitional pathway, I chose one skill, one stuff and one image from the practice to analyse which factors could lead for a change. They were:
<br>
<br>
*Skill: sourcing*
<br>
• raw material provenance
<br>
• reuse/recycling
<br>
• life cycle assessment on design stage
<br>
• circular economy transition
<br>
• bioengineering
<br>
• new materials from “waste”
<br>
• economical incentives for fair trade/circularity
<br>
<br>
*Stuff: Machines*
<br>
• accessibility in public coworking spaces
<br>
• open development for simplifying systems
<br>
• incentives for open software as well
<br>
• digital literacy
<br>
• additive technologies development
<br>
<br>
*Image: single use*
<br>
• legislation ban into plastics
<br>
• change of material to biodegradable
<br>
• reusable objects/kit (new products development)
<br>
• sharing platforms
<br>
• make your own - mentality shift over duration of things
<br>
<br>
From this exercise of thinking of the current state of a practice and projecting the desired one for the future, creating the links between them by a set of actions as transitions, it was possible to visualize way much better how to achieve a change. At the beginning I struggled in making the scenario for the current practice, as I only could envision the negative aspects of it and the ones that I would like to change. Then I had a talk with Cormenzana that made me visualize that I could also add aspects that are questionable but positive of the current state of *producing*. The activity was interesting to be done as it gave a sense of reality in the possibilities of change, instead of keeping us in the utopian space of discussion of better futures yet being unable to verbalize the steps for it. The class gave us tools to find the words and paths for proposing the changes.
<br>
<br>
The third and final session was about performing the desired new scenario and prototyping the idea for improvements. In the class, we simulated a scenario including the project of one of our colleagues, Adriana Tamargo, which deals about exclusion and bullying at young ages.
<br>
<br>
{% figure caption: "*theatralization of the scenario*" %}
![]({{site.baseurl}}/dfn_w01_04.jpg)
{% endfigure %}
<br>
<br>
For my project, prototyping really meant doing. Since I want to talk about cultural expressions and material culture evolution, I wanted to see how new technologies could interact with the Commons knowledge of the craftsmanship. As a way to start, I defined to look into a specific object that express culture. I decided to look into plates, since food is the most clear cultural trace and one of the best acts of connection between people. Each culture has its own way of display food - based on the availability of ingredients during seasons, of scarcity or abundance. Thus, I want to shed light on the most candid support (plates) for the most direct cultural expression (food) with a design intervention. My idea was to look into different cultures and how they display food. By that, I would get inspiration from traditional pottery making and I selected three locations to start: Serra da Capivara in Brazil, Mashiko in Japan and Fez in Morocco. I analysed how pottery workshops make the tableware in these locations and took the measurements and shapes of one piece of each. Then, with the help of a Fab Academy tutor, Eduardo Chamorro, I developed a parametric design that could change accordingly to the parameters of each culture, plus considering the local material - each clay has a percentage of shrinkage, and this can also be taken into account for the design.
<br>
<br>
{% figure caption: "*First design ideation*" %}
![]({{site.baseurl}}/dfn_w01_05.jpg)
{% endfigure %}
{% figure caption: "*Second design ideation*" %}
![]({{site.baseurl}}/dfn_w01_06.jpg)
{% endfigure %}
<br>
<br>
With the designs more or less constrained by the limitations of ceramic 3D printing, we did the first trials of printing. With this exercise, it was possible to realise that a real-sized plate would not be possible to be made with the equipment available in the lab. First, I did scaled versions, then I moved on to print portions of the designs as archaeological artifacts, in order to study the possibilities and constraints this technology brings.
<br>
<br>
![]({{site.baseurl}}/dfn_w01_07.jpg)
{% figure caption: "*Experiments with clay extrusion*" %}
![]({{site.baseurl}}/dfn_w01_08.jpg)
{% endfigure %}
<br>
<br>
The whole idea is to thrive local connection through making, by linking traditional pottery making regions with the new makerspaces and explore the possible learnings each can have, bringing this new set of knowledge to the community. I learned that prior to projecting a change of production, which is very much possible, I first need to focus on literacy of new technologies and synergies between the two different sets of knowledge. After this process, it is possible to achieve a change in the way we produce, fostering the making in the urban context. A next iteration of the project would be to establish a methodology to bring the two sets of knowledge - craftsmanship and digital fabrication - into a workshop to see what is comprehensive or not  for a community.
<br>
<br>
With the *Design for the new* course, it was possible to apply a new look into the role of the design in our everyday lives, and how that our choice for an intervention can be defined through many steps that will be developed along a pathway, creating a gradual change in the way societies behave. This methodology introduced to us by Rua and Cormenzana enables a strategic analysis of the set of habits and rituals that societies have been perpetuating and that are evolving through times. Moreover, it brings ways to place our questioning and then later a proposition for new and better futures.

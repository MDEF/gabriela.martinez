---
layout: page
title: About
permalink: /about/
---

<br>
<br>
Gabriela Martinez Pinheiro/Simões Garcia graduated in Architecture and Urbanism (FAUUSP, 2006-12 and ETSAM-UPM, 2010-11) and has a master in Design for Emergent Futures (IAAC/Elisava-UPC, 2018-19). Previous and current projects address topics related to ecodesign, urban regeneration, participatory design, digital fabrication, material culture and shared knowledge. As an architect always exploring new crafts and strategies, believes in the power of making as a tool of connection and awareness, both much needed in our times.

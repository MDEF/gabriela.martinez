---
layout: home
---

![](images/questions_anne-collier.jpg)
<br>
<br>
Views of a student from the Master in Design for Emergent Futures (MDEF) by Institute for Advanced Architecture of Catalonia (IAAC) and ELISAVA Barcelona School of Design and Engineering.

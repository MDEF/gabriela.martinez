---
title: 03 • Material Driven Design
period: 04-08 January 2019
date: 2019-02-08 12:00:00
term: 2
published: true
---


<br>
<br>
As a first approach to work with the selected material - waste from avocado, the peels and seeds - I decided to manipulate them during the first week only by their characteristics, without adding any other binder or plastifier. I decided to analyse the materials in three different starting points: fresh (slightly wet), dried (naturally or in the oven) and wet (from letting it submersed in water for one day).
<br>
<br>
I started with the fresh peels, sensing their resistance to break and analyzing their structure. I could notice that while breaking the skin, I could not control much the direction, since it was really soft to break. Also, there were no signs of long fibers on it. I decided to break them in different sizes and check if they would bind together again, also cooked with water to see possible changes. I have not got much interesting results from this process, the pieces did not bind together back.
<br>
<br>
The process of manipulating the seeds ended up being more interesting. First, I shredded the fresh seeds on the Precious Plastic shredder machine. At this stage, I left some pieces to dry naturally to see what would happen on next day (S-003).The remaining content, I blended it into a flour with a mixer. With this flour, I decided to cook it in different proportions material-water to see its consistency. Also varied the amount of time for the cooking. It was interesting that I could cook the material into a sort of pudding, which actually could be molded. The amount of water was quite big in proportion, so it took a lot from the samples to dry naturally and most of them eventually got mold.
<br>
<br>
Since working with the seed seemed more promising at this point, I tried to focus my next experiments in exploring its possibilities - these are described in the next post. The recipes for this first contact with the materials are described below.
<br>
<br>
 {% figure caption: "*Tests with the avocado peel*" %}
 ![]({{site.baseurl}}/mdd_w03_01.jpg)
 {% endfigure %}

 ![]({{site.baseurl}}/mdd_w03_p001.jpg)

 ![]({{site.baseurl}}/mdd_w03_p002.jpg)

 ![]({{site.baseurl}}/mdd_w03_p003.jpg)

 {% figure caption: "*Tests with the avocado seed*" %}
 ![]({{site.baseurl}}/mdd_w03_02.jpg)
 {% endfigure %}

 ![]({{site.baseurl}}/mdd_w03_s001.jpg)

 ![]({{site.baseurl}}/mdd_w03_s002.jpg)

 ![]({{site.baseurl}}/mdd_w03_s003.jpg)

 ![]({{site.baseurl}}/mdd_w03_s004.jpg)

 ![]({{site.baseurl}}/mdd_w03_s005.jpg)

  ![]({{site.baseurl}}/mdd_w03_s006.jpg)

 ![]({{site.baseurl}}/mdd_w03_s007.jpg)

 ![]({{site.baseurl}}/mdd_w03_s007_1.jpg)

 ![]({{site.baseurl}}/mdd_w03_s007_2.jpg)

{% figure caption: "*Took sample S-007 out of the mold*" %}
 ![]({{site.baseurl}}/mdd_w03_s007_3.jpg)
{% endfigure %}

 {% figure caption: "*All samples*" %}
 ![]({{site.baseurl}}/mdd_w03_03.jpg)
 {% endfigure %}

---
title: 02 • Design Studio
period: 29 January 2019
date: 2019-01-29 12:00:00
term: 2
published: true
---


<br>
<br>
On the second class of Design Studio, we had desk critics with tutors Tomas Diez, Mariana Quintero and guest tutor Thomas Duggan.
<br>
<br>
To organize my ideas about the final project, I structured it around the 3 main topics inside the Application track of the masters, which this subject is part of. **Understand**, **propose** and **develop** are these topics and where deployed in the presentation below.
<br>
<br>
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRdZ454HZQgNsP91vDhlVutlrTGvChzLw8hLDL9-x2pcN2PmGnVsxZrbi5nbz2KR8s-Q9jTB-7rkVvE/embed?start=false&loop=false&delayms=3000" frameborder="0" width="680" height="424" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<br>
<br>
I showed this presentation to Thomas Duggan and we talked about the project. I received a really interesting feedback, which is commented below:
<br>
<br>
•
<br>
<br>
•
<br>
<br>
•
<br>
<br>
•
<br>
<br>
•
<br>
<br>

---
title: 02 • Material Driven Design
period: 31 January -01 February 2019
date: 2019-02-01 12:00:00
term: 2
published: true
---


 <br>
 <br>
 For the second week of the course *Material Driven Design* we were asked to source raw materials to develop a new biomaterial that would be fully biodegradable. We would need to have a relatively big source from this material, that could be a restaurant or factory waste. Then, we were asked to do a research about the material: its history, where it comes from, how has been manipulated, the anthropological side, its chemical composition, the technical and experiential aspects of it.
<br>
<br>
I decided to visit a food market to spot possible waste materials there. The markets here have specific rules regarding the discarding of waste, and every food/fruit kiosk ends up adding their waste to a communal container, which would make it difficult to source a specific item. This made me miss being in São Paulo, where street food markets leave the food to be picked by the cleaning staff, so in this context, to gather a material would be much easier.
<br>
<br>
{% figure caption: "*Visit to Mercat Santa Caterina*" %}
![]({{site.baseurl}}/mdd_w02_01.jpg)
{% endfigure %}
<br>
<br>
I visited another market and could collect some materials with Barbara and Veronica. None of us were much excited about the materials collected, so we kept on the search for a bigger supply of materials
<br>
<br>
{% figure caption: "*Some materials collected at Mercat La Boqueria*" %}
![]({{site.baseurl}}/mdd_w02_02.jpg)
{% endfigure %}
<br>
<br>
The next tentative was to contact restaurants and see if they could collaborate. Some were really nice and proactive, separating some items from the normal trash. The fact that normally all gets mixed in the discard of organic waste was another barrier discovered. From the items available in one of the restaurants that decided to collaborate, [Bohl](https://bohl.co/), I have got the idea of working with avocado waste. Later I found a restaurant in Barcelona that dedicates a good amount of its menu to avocado, the [A Vocados](https://www.facebook.com/avocadosbcn/), and they were really nice to accept to collaborate as well.
<br>
<br>
{% figure caption: "*Avocados peels and seeds collected from restaurants - these are imported from Mexico*" %}
![]({{site.baseurl}}/mdd_w02_03.jpg)
{% endfigure %}
<br>
<br>
I started to research about the avocado and discovered many interesting things concerning the most common specimen that we consume, the Hass avocado. I guess the most interesting non-scientific article that I read was published by Smithsonian Magazine: [Holy Guacamole: How the Hass Avocado Conquered the World](https://www.smithsonianmag.com/science-nature/holy-guacamole-how-hass-avocado-conquered-world-180964250/). There, I discovered many peculiarities of the Hass Avocado, as for example, it was patented and that is from where its name comes from. For the scientific information, I will write below with the corresponding references:
<br>
<br>

### Context
<br>
<br>
“Avocado (Persea Americana Mill., Lauraceae) is an important fruit native to Central America and Mexico and cultivated in almost all tropical and subtropical regions worldwide. The world production of avocado was around five million tons in 2014. The largest producer of avocado is Mexico, country that accounts for 30% of the world production. Brazil produced 157,000 tons in 2014 [1, 2]. Avocado fruits have high nutritional quality and contain high levels of vitamins, minerals, proteins, and fibers, as well as high concentrations of unsaturated fatty acids, beneficial to health [3]. In addition, avocado peel and seed have high contents of bioactive phytochemicals such as phenolic acids, condensed tannins, and flavonoids, including procyanidins, flavonols, hydroxybenzoic, and hydroxycinnamic acids [4–6]. These bioactive compounds have shown various biological activities such as antioxidant and anti-inflammatory properties. The antiinflammatory activity of phenolic compounds is largely related to their ability to scavenge oxidative radicals, which is important for cell and oxidative stress regulation [7].
<br>
<br>
Different varieties of avocado are cultivated worldwide, and Hass and Fuerte are among the most consumed ones [2]. Avocado industrial processing generates large quantities of agroindustrial by-products, mainly peel and seed, ranging from 18% to 23% of fruit dry weight [8]. Thus, it is interesting to reuse these by-products both to reduce their negative impact on the environment and to add value to them, inasmuch as they are sources of important phytochemical compounds [9, 10]. Therefore, the transformation of these agro-industrial by-products, which yield a range of bioactive substances, should be made by green extraction to reduce the environmental impact of the agro-industrial chain.”
<br>
<br>
Source:  Maria Augusta Tremocoldi, Pedro Luiz Rosalen, Marcelo Franchin, Adna Prado Massarioli, Carina Denny, Érica Regina Daiuto, Jonas Augusto Rizzato Paschoal, Priscilla Siqueira Melo, Severino Matias de Alencar. [Exploration of avocado by-products as natural sources of bioactive compounds.](https://journals.plos.org/plosone/article/file?id=10.1371/journal.pone.0192577&type=printable)
<br>
<br>
### Avocado Skin
<br>
<br>
“The skin of the avocado fruit consists of an epidermis, together with hypodermis chlorenchyma and sclerenchyma tissues. The epidermal layer, one cell thick, extends completely over the fruit surface and is broken only by lenticel formation which eventually may develop into rather extensive corky areas (Figures 1 and 2). Such corky areas may cover some fruits to variable degrees of completeness. The epidermis consists of rather regular cells, block-shaped in cross section and irregularly polygonal in transverse section. Cell division occurs in this and most other tissues in all parts of the fruit pericarp throughout its life. Thus cell division, both radial and tangential, allows for expansion of the fruit in all planes. The development of a hypodermis one or two cells in depth generally is observed.
<br>
<br>
The outer tangential cellulose wall of the typical epidermal cell is heavily cutinized asevidenced by the staining reaction with Sudan III. This cutinization may extend laterallyalong the radial walls and even continue into the tangential and radial walls of thesecond or third cell layer below the epidermis. Such extensive cutinization is evident usually in older and mature fruit and is quite variable in extent.
<br>
<br>
Source: C. A. Schroeder. The Structure Of The Skin Or Rind Of The Avocado.
<br>
<br>
### Anthropological context
<br>
<br>
The rise in the consumption: “(...) people's greater interest and awareness of healthy eating, the improvements that have been made in the supply chain in areas like ripening and grading, the production shift towards to one variety in particular, Hass, and yes, the sheer popularity of guacamole. For some, the fact that I didn’t mention the potential downsides of the avocado trade’s recent explosion – massive water usage, or the potential for unethical treatment of workers in certain production areas – might also be a concern.”
<br>
<br>
Source: [Why have avocados become so popular?](http://www.fruitnet.com/eurofruit/article/172578/why-have-avocados-become-so-popular) by Mike Knowles
<br>
<br>
“Social media has created a buzz about avocados as a creative, wholesome and healthy variant. Awareness and availability have especially the millennials in the city experimenting with this superfood,” notes Seth.” Quote from chef Vikas Seth.
<br>
<br>
Source: [The rise of avocado: How restaurants have seen a 100% jump in demand for fruit in two years](https://economictimes.indiatimes.com/magazines/panache/the-rise-of-avocado-how-restaurants-have-seen-a-100-jump-in-demand-for-fruit-in-two-years/articleshow/64379971.cms) by Smita Balram.

---
title: 01 • Design Studio
period: 22 January 2019
date: 2019-01-22 12:00:00
term: 2
published: true
---

<br>
<br>
In the first class of Design Studio the students presented the projects developed in Barcelona and China. The projects were discussed and we were asked to document it on our website. Here I present the work developed during our week in Barcelona: *Make Here Barcelona*.
<br>
<br>
Make Here is a project to share information about local making and create a community. We want to connect people who make to people wanting to make. It started as an one-week project done by six students of the Master in Design for Emergent Futures (MDEF) from IAAC, inspired by [Make Works](https://make.works/) and [Fab City](https://fab.city/). Initially, a mapping of information about maker’s spaces, material suppliers, workshops and other creative places was done in the neighborhood of Poblenou, in Barcelona. The idea is that anyone can go through the same process and develop the project in their own cities by learning from a toolkit. A global network can be developed and communities can learn from each other. Thereby, people can rethink their relationships with their localities and collaborate for changes towards a more circular economy.
<br>
<br>
This project was done by Aleksandra Łukaszewska, Alexandre Acsensi, Emily Whyman, Gabriela Martinez, Jessica Guy and Veronica Tran. In the toolkit we explain that our initial step was to visit makers we already knew and let them guide us to new maker's spaces in Poblenou. The result of this process can be seen in the video below.
<br>
<br>
<iframe src="https://player.vimeo.com/video/313129045" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/313129045">Make Here BCN</a> from <a href="https://vimeo.com/user94271357">Make Here</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
<br>
<br>
We also created a website and an instagram account to spread the idea and engage people in taking part on the project, here in Barcelona or in their own cities.
<br>
<br>
Website:[https://makehere.gitlab.io/make-here-bcn/index.html](https://makehere.gitlab.io/make-here-bcn/index.html)
<br>
<br>
Instagram: [https://www.instagram.com/makeherebcn/](https://www.instagram.com/makeherebcn/)
<br>
<br>
In the website, people can download the toolkit that explain the steps to recreate the project. We already have some colleagues enganging in the idea of making the project in their hometowns. Moreover, we surely will keep with the project, exploring new spaces, creating the network and community around it and refining the methodology for the project.

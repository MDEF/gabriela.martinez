---
title: 03 • Design Studio
period: 05 February 2019
date: 2019-02-05 12:00:00
term: 2
published: true
---


<br>
<br>
This week we had classes at Elisava, as we were visiting the exhibition [*Design for City Making (DXCM)*](http://www.elisava.net/en/research/projects/design-for-city-making).
<br>
<br>
Dr. Oscar Tomico presented us the projects in the exhibition in more detail. The projects were categorized in different...


<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTOjgfD1SIYt8VcwxTrCfQzu7wnBMDkEJne8u4Z7wVaKjPyPSxKghFy3i99sWVon3Ny9m1ALVGVi50t/embed?start=false&loop=false&delayms=3000" frameborder="0" width="680" height="424" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

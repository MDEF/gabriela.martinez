---
title: 06 • Designing with Extended Intelligence
period: 05-09 November 2018
date: 2018-11-11 12:00:00
term: 1
published: true
---

<br>
<br>
*Designing with Extended Intelligence* (05-09.11.2018) was a week of exploration of topics related to artificial intelligence and machine learning with Ramón Sangüesa Sole and Lucas Lorenzo Peña as tutors. Sangüesa works with technology and social innovation and Lorenzo with interaction design and social understanding on neuroscience. The goal for the week was to provide us a foundational understanding of machine intelligence, its tooling and ethics, so we can envision the design of products, apps, services or systems that can leverage and extend our own intelligence with those machines.
<br>
<br>
It was important to discuss the role of the designer in this process, since all depends on the intent of the developer of such machines to guide the purpose of its use. Media has been devoting much attention to the misuse of such technologies, as like the influence over voter’s decisions. We obviously need to be aware that machines are being developed to influence or make decisions for us and, as I pointed previously on the [Navigating the Uncertainty documentation](https://mdef.gitlab.io/gabriela.martinez/reflections/navigating-uncertainity/), this kind of control might impact our democracy. Hence, there is a huge field of exploration in matters of building trust and transparency around this technology.
<br>
<br>
As a way to approach the concept of artificial intelligence, we first reflected over the concept of intelligence, discussing it in groups and later sharing our insights. Our group agreed that *intelligence is sometimes perceived as cognitive ability, however its definition changes in different contexts and societies. Intelligence is the ability to process, connect and share information. It is a physical response but also psychological, however is also a response to our environment and cultural norms*. Plato and Aristotle interpretations of intelligence were commented and we advanced on the topics of symbols and its interpretations. The complexness of approaching definitions and measurements of human intelligence also extend in the field of technology.
<br>
<br>
The context for the first conceptualization of artificial intelligence was presented to us. The Dartmouth Conference presented a proposal to the U.S. government over *Automatic Computers*. After getting in touch with these first concepts, we gathered in groups to think about an intelligent object and what were its goals, type of tasks, inputs, outputs and context of operation. Throughout the week, we went back to this idea of project to discuss what kind of methods of machine learning we could apply, if neural networks could be a useful learning tool and topics related to ethics. It was interesting that during the same week we had a lecture at IAAC with the artist [Kyle McDonald]( http://www.kylemcdonald.net/) about Computer Vision & Machine Learning for Interactive Art. He pushes the boundaries about image, identity, creation of virtual realities and new relations that arise of the use of technology. When we see the potential of this technology applied in the art field it becomes easily understandable all the social, cultural, technical and even economic implications its use can bring. It was really interesting to get in touch with his opinion that technology can be an open space for mistakes and errors that lead us to have a softer vision of ourselves.
<br>
<br>
{% figure caption: "*The Infinite Drum Machine - an experiment on how machine learning might organize everyday sounds. Project developed by Kyle McDonald, Manny Tan, Yotam Mann with Google Creative Lab.*" %}
![]({{site.baseurl}}/w06_00.jpg)
{% endfigure %}
<br>
<br>
When we learned about the basic types of machine learning and the different methods to work with data, it became really clear the importance of how the data is labeled and classified by then the machine operate with it. Since machines work in understanding symbols, we need to be very aware of how we inform them about these symbols. It is the work of the developers to define concepts and, sometimes, to define these concepts in real life is already a hard task. In addition, when working with data for the machines, it also reflect aspects of our societies and their mentalities. That is when machine can present bias, reflecting negative aspects of our society as like prejudice among ethnicities and gender. Acknowledging those biases in the real world and tackling them to not transmit it in the data is of major importance.
<br>
<br>
The ethics around artificial intelligence and machine learning go beyond topics like discrimination and include various concerns around our societies. Privacy is another major concern, as more and more big corporations have access to our personal information for free and are manipulating it in unclear ways. For the moment, most of the information is about our identity and interests, but what will happen in the future with the rise of biometric and health data collection? How companies will use biological information about us?
<br>
<br>
Therefore, we need to be aware of the implications of the use of these new technologies and help to educate people about it. During a talk I attended the same week about the [Sustainable Development Goals and their repercussion in the future of the cities]( https://www.barcelona.cat/smart-city-week/es/programa/seminari-objectius-de-desenvolupament-sostenible-ods-i-la-seva-repercussio-en-el-futur-de-les-ciutats/) at the Smart City Week 2018, Gerardo Pisarello, part of Barcelona’s municipality team, raised the attention for the rise of fear in cities, which part it is related to the use of new technologies. Pisarello mentioned that besides the concern of social control with the implementation of machines, people are worried with the lack of humanization and the future scenario where machines will take their jobs. Consequently, it is majorly important that new technologies become a tool to help society achieve new standards of development in our cities but not annihilate its human character.  
<br>
<br>
To conclude, I want to share a recently released video by [A/D/O and Somesuch](https://a-d-o.com/journal/ai) called *Design & the revolution of AI*. It is a video that exposes topics around the incorporation of this technology in our lives in the way I commented previously, looking to potentialize humans’ abilities and capacities rather than to overcome us. I also want to share some of the opinions expressed in the video in a written format:
<br>
<br>
“We have to be really careful when we’re talking about technology that we don’t separate it out as this separate thing from the rest of life. We blame an awful lot on technology that really should be something that is used to inspire both personal introspection and cultural introspection.” *Ben Hammersley – Technology Journalist*
<br>
<br>
“If you ask an AI to design a structure it might well come up with something that looks like a tree or a plant, because those are efficient structures that have evolved over millions of years. Also with furniture design maybe in the 70’s and 80’s you had this trend for organic forms, and mimicking nature. And now because of different ways of manufacturing using 3D printing you have much more organic structures. It does seem to be not only the right thing structurally or mathematically but it seems like it’s an aesthetic which people like and which we associate with the future.” *Jon Marshall – Co-founder and director of Map Project*
<br>
<br>
“If you look at mathematics, a tremendous amount of nature is based on mathematical principles, whether it be the Fibonacci series or fractal designs and things like that. You see fractals everywhere and these are basically mathematical constructs. For AI to mimic certain natural processes is absolutely no surprise to me. I think it was bound to happen because it’s the nature of the universe.” *Nolan Bushnell – Founder of Atari*
<br>
<br>
You have computer-generated painting. You have computer sculpting. So where is the value still if a computer is able to generate a really complex piece of music in a second? I personally always believed it’s still worthless if a computer makes it, if you don’t know who is behind it and the story behind the person who actually created it. The connection from human to human is what makes things interesting in life.” *Joris Laarman – Artist, Jorris Laarman Lab & MX3D*
<br>
<br>
“The role of design has always been to be an agent of change that interprets change in all sorts of different areas of our lives to ensure that we can turn them to our advantage rather than them affecting us to our detriment and artificial intelligence will be no different.” *Alice Rawsthorn – Author of Hello World: Where Design Meets Life*
<br>
<br>
<iframe src="https://player.vimeo.com/video/300550003?color=030004&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/300550003">A/D/O and Somesuch Present: Design &amp; The Revolution of AI.</a> from <a href="https://vimeo.com/adonyc">A/D/O</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

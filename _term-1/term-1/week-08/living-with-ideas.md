---
title: 08 • Living with Ideas
period: 19-23 November 2018
date: 2018-11-25 12:00:00
term: 1
published: true
---

<br>
<br>
*Living with ideas: temporal aspects of embodied living with prototypes* (19-23.11.2018) was a week tutored by Angela Mackey and David McCallum, both affiliated to Eindhoven University of Technology. They work on the intersection of art, design and technology; Mackey has a background in new media and interaction design and now focus on wearables and embedding digital in fabrics, and McCallum has a background in music and identifies himself as an artist, working with technology and interaction.
<br>
<br>
First, we learned about their trajectories and projects, McCallum is a Fab Academy alumni, having developed a device to turn a machine into a performable instrument and also did a PhD in Sweden, *Glitching the Fabric*, in which he developed a digital generator of patterns based on traditional patterns. Mackey developed garments to be wear on space and also created a brand of light clothing for safety cycling, called [Vega]( https://www.angellamackey.com/vega-wearable-light/)
<br>
<br>
Later, we did an activity based on a methodology developed by Ron Wakkary. Before the class, we read his article *A short guide to material speculation: actual artifacts for critical inquiry* to get prepared. We were introduced to the concept of material speculation through design of counterfactual artifacts for an inquiry of possible worlds.
<br>
<br>
Ron Wakkary, William Odom, Sabrina Hauser, Garnet Hertz, and Henry Lin. 2016. A short guide to material speculation: actual artifacts for critical inquiry. <em>Interactions</em> 23, 2 (February 2016), 44-48. DOI: https://doi.org/10.1145/2889278
<br>
<br>
The methodology is a technique to bring the future, or a future concept, to the present time so that we can observe it and live with it. By living with it, we experience and learn things about the context of this possible future.
<br>
<br>
Each student brought an object to the class; we disposed them on a table and should select an object brought by a colleague. The task was to design a counterfactual artifact to speculate about a possible world, different from ours.
<br>
<br>
I selected a spoon and my first inquiry around it was:
<br>
<br>
*In a future of scarcity, what if we could not collect or accumulate things?*
<br>
<br>
My first idea was to transform the spoon in a flat object, so it would withdraw the ability to collect things. However, I realized that this change would take out the fundamental concept of a spoon and would not help me develop a tool for inquiry of this new possible world. Therefore, I transformed the spoon in a sort of cocoon, creating a limited space for collection of material.
<br>
<br>
{% figure caption: "*Object selected and first idea for a counterfactual artifact*" %}
![]({{site.baseurl}}/w08_01.jpg)
{% endfigure %}
<br>
<br>
This counterfactual artifact restrains the amount of substance collected and end up giving a more controlled way of measuring content. It also lead people to change the gesture of collecting things. It becomes a more precise tool for controlling/measuring substances. From living with it, I learned that the natural movements to manipulate a spoon would change, more horizontal and vertical moves would be needed instead of “digging” and “stirring” ones. It could be hard for people with coordination disorders to achieve collecting something but probably if attached to a machine, like a robotic arm, it would be easier to maintain the content in the spoon since by the nature of the moves and the enclosed shape.
<br>
<br>
{% figure caption: "*The second idea for the counterfactual artifact and living with it*" %}
![]({{site.baseurl}}/w08_02.jpg)
{% endfigure %}
<br>
<br>
After our personal explorations with the counterfactual artifact, we gathered to draw conclusions of the experience. It was interesting to notice how body language can be transformed and new gestures be created. To experience a potential real feeling that people can have in this new context. To find errors or flaws in existing tools and all the possible situations where the speculation will not work. Among many other topics, I would highlight one more, which is to create new forms of communication, a new language and interactions.
<br>
<br>
On the second day, we could learn better about the research project Mackey developed for her PhD. She wanted to learn more about wearable design and how people enjoyed the idea but was not incorporating it into life. Wearables were not being translated in real world. Therefore, she decided to have a day-to-day experience to understand the technology. Mackey did a project to daily use a garment in green color that she could turn it into *dynamic fabric* with the aid of a chroma key app. She defines *dynamic fabric* as fabric that visually changes according to computing input. She used autoethonography as methodology, kept a diary of the experience and through videos and photos, she shared it with an audience during one year.  From living this experiment, she discovered new perceptions and feelings. The second part of her PhD was to go through a fashion design process. Mackey created a fashion brand incorporating the concept and worked on a fashion film to create an identity.
<br>
<br>
The workshop of the day was for us to experience autoethnography through the same process. So we experienced being wearers of *dynamic fabrics*. We should reflect of how we would like to wear it, what we would wear and when and why we would wear. Later, we did the experiment of wearing the *dynamic fabrics* and we should reflect on how we felt of wearing it. For me, it was interesting to see the difference between the pattern being applied to my sweater, which is dark green, to the effect in the bright green fabric I turned in a scarf. The sweater received a subtle overlay while the scarf was completely changed by the pattern. I played with different patterns, like flooring tiles, PCB board and other fabrics. It was interesting to notice that depending on the image applied as a pattern, the lighting and shadow of the real object would be overlapped. I shared one of the photos on my Instagram stories and it was curious to receive some reactions from it, including people that thought it was a real printed fabric. When I clarified, this person expressed that she thinks that instead of *digital fabric* this should be called *virtual fabric*.
<br>
<br>
{% figure caption: "*Wearing dynamic fabrics*" %}
![]({{site.baseurl}}/w08_03.jpg)
{% endfigure %}
<br>
<br>
Mackey concluded the day by explaining to us how she managed to explore an emerging technology that does not fully exist yet. She initially wanted to explore a fabric to touch, but then she searched for the essence of the idea. By doing autoethnography and a lived experienced of a real technology, she was able to learn from the experience and explore a system and a technology that already works.
<br>
<br>
David McCallum guided us on the third day of workshop, presenting us with 16 human desires that we should address to make a magic machine. The first machine should be done based on a desire that interest us or disgust us. The second machine should be addressed by a desire related to our field of interest.
<br>
<br>
For the first machine, I chose a topic that interests me, *eating – the need of food*. My first inquiry around the topic of *eating* was about how social disparities are reflected in the way we feed ourselves. For this magic machine, I imagined then a machine that could provide people with a basic nutrition, giving the needed amount of vegetables, carbohydrates and proteins. The *basic nutrition* machine would materialize this food through magic inside each compartment and it could be in solid or vapor state. The process of creating this machine was not much smooth for me, I struggled for a while to materialize a concept around the need for food, since when I first chose this topic, I was expressing my interest more around the social aspect of eating, not much on the practical aspect around it.
<br>
<br>
{% figure caption: "*The ‘basic nutrition’ magic machine*" %}
![]({{site.baseurl}}/w08_04.jpg)
{% endfigure %}
<br>
<br>
For the second machine, I selected the human desire of *social contact – the need for relationship with others*. Since my topic of interest has been revolving around social aspects, as the way we place and deploy our rituals, I thought this was a human desire aligned to it. For this exercise, I designed the *Augmented connection* magic machine. By fixing around your hand and a friend’s hand that can be far away, it would make possible a connection between the two people by aligning hands. Through magic, they would be able to share good feelings with each other.
<br>
<br>
{% figure caption: "*The ‘augmented connection’ magic machine*" %}
![]({{site.baseurl}}/w08_05.jpg)
{% endfigure %}
<br>
<br>
During the next two days, we had to apply one or more methodologies that we have learned in this week to our area of interest or specifically the project we had in mind. Since I have been interested in the way of how society has been shifting the practice of rituals, making it celebrations with more and more inorganic artifacts, I tried to apply methodologies to dive deeper in this topic. Since we approaching Christmas time, I first did an exploration to a store to see what items related to Christmas are already being sold. I have got really interested in reading the product’s labels, to see their origin and composition. All the products in that store were made in China. In addition, all labels were inaccurate, pointing out just one or two materials that compound the product instead of specifying all materials used in it. This lack of transparency on the material information really caught my attention. Also, I have got intrigued to see how many plastic/synthetic products made references to nature’s elements. Moreover, the use of concepts or words related to nature or sustainability, as plastic “growing” figures or “solar cell” figures, which are only inutile plastic decorative artifacts.
<br>
<br>
{% figure caption: "*Christmas artifacts being sold*" %}
![]({{site.baseurl}}/w08_06.jpg)
{% endfigure %}
<br>
<br>
From this visit to the store, I started to think how people could celebrate in a more organic way. My idea for the exercise was then to develop a mockup of an app where people could find recipes to more sustainable alternatives. Then, I searched for recipes and created a basic initial interface for this speculative artifact. With some ideas of how to alternatively celebrate, I decided to put one of the “recipes” in practice. In opposition to the plastic bow tie found in a store, I decided to do a natural bow tie. I collected leaves from the streets, nearby IAAC, and with a jute rope, I tied them as a bow tie. My experience of doing this artifact was really smooth and quick, it didn’t took me a lot of effort to materialized it. However, I wonder if people would actually care to start doing their own artifacts for rituals instead of simply and easily buy them.
<br>
<br>
{% figure caption: "*Mockup for the app*" %}
![]({{site.baseurl}}/w08_07.jpg)
{% endfigure %}
<br>
<br>
{% figure caption: "*Putting in practice one of the “recipes”*" %}
![]({{site.baseurl}}/w08_08.jpg)
{% endfigure %}
<br>
<br>
From the closure discussion with the visiting tutors, Mariana Quintero and Dr. Oscar Tomico, all the people that are in somewhat dealing with the topic of sustainability were asked to reflect on their own habits and behaviors first than expecting people to act differently. Moreover, sustainability is one of many variables in a system, and we should start to understand the whole chain of production to achieve a circular economy. Current society and business models will never be circular. However, how can we start to make a change on it? A first step would be building a community, making possible to explore future ways of using resources.
<br>
<br>
### Conclusions
<br>
<br>
Learning about different methodologies throughout the week gave us new tools of exploration towards our areas of interest. It was interesting to get in touch with Angela Mackey and David McCallum’s approaches of research through design and how to experience it. It made possible to envision future paths for the project, the ones I think I do not want to take it and the others that might be worth exploring. It was a challenging week in which we had to confront ourselves with our ideas and our interests for the research project that will be developed in the near future.

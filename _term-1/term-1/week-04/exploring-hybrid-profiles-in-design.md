---
title: 04 • Exploring Hybrid Profiles in Design
period: 22-26 October 2018
date: 2018-10-28 12:00:00
term: 1
published: true
---

*Exploring Hybrid Profiles in Design* (22-26.10.2018) was a week in which we visited three different practices and were able to hear from the designers about their trajectory and actual work. In each visit, we also went through an activity proposed by the designer. Later on the week, we had classes with Dr. Oscar Tomico to improve concepts over our own identity and vision.
<br>
<br>
### Studio Visits
<br>
First, we visited [Domestic Data Streamers]( https://domesticstreamers.com/), a consultancy design firm. There, we had the opportunity to talk with the designers Pol Trias and Marta Handenawer. Both graduated from Elisava and went through product design and/or engineer and the work with data emerged from personal interests. A project after the other, more people were reaching them for development in the data field crossing information and human emotions. The company has now 5 years of existence and works with brands from public and the private sector developing storytelling through data sharing. The team is multidisciplinary, compounded of designers, social scientists, engineers and creative technologists. They presented several projects to us and did an activity that consisted to select some things we carry on our bags that would tell something about us. We organized our objects, developed charts and explained their personal meaning.
<br>
<br>
{% figure caption: "*Domestic Data Streamers  .  working tools [patterns of organization]*" %}
![]({{site.baseurl}}/w04_01.jpg)
{% endfigure %}
<br>
<br>
{% figure caption: "*Domestic Data Streamers  .  activity of personal meaning*" %}
![]({{site.baseurl}}/w04_02.jpg)
{% endfigure %}
<br>
<br>
Secondly, we visited Ariel Guersenzvaig, a former usability consultant now design researcher on ethical digital transformation. He explained the beginning of his career on the rise of internet and the huge demand for web designers at that time. Besides designing and managing, he started to teach and research more over the time. After doing his PhD with a thesis on Design decision making, he directed his interest towards professional ethics. The activity proposed by him was to discuss the implications of the use of killer robots. Questions about the lack of human decision on the act of killing and the responsibilities of a wrong death caused by a robot were much debated.
<br>
<br>
Finally, the last visit was to the studio of [Sara De Ubieta](http://www.deubieta.com/). She is an architect that also developed a passion for shoe making. After doing a course on traditional shoe making, she found her path on the design field instead of in the architecture field. After doing many pairs of shoes in leather, she wanted to explore other materialities and started to research on new materials. Besides managing her own brand, Sara teaches, develops research with institutions and does collaborations on the arts field. The activity with her was to design a shoe from reused cotton fabric.
<br>
<br>
{% figure caption: "*Sara De Ubieta studio  .  shoes and references [patterns of organization]*" %}
![]({{site.baseurl}}/w04_03.jpg)
{% endfigure %}
<br>
<br>
{% figure caption: "*Sara De Ubieta studio  .  activity of shoe making*" %}
![]({{site.baseurl}}/w04_04.jpg)
{% endfigure %}
<br>
<br>
{% figure caption: "*Sara De Ubieta studio  .  result of the activity of shoe making*" %}
![]({{site.baseurl}}/w04_05.jpg)
{% endfigure %}
<br>
<br>
### Acknowledgements from the Visits
<br>
From the visit to designers during the first part of the week, I visualized skills, knowledge and attitudes that I admire and want to develop better in my practice. For a more objective visualization, I wrote down it into topics:
<br>

• Make a hidden info or fact visible to people (open people’s eyes to a topic that it is not that visible).
<br>
• Create a reflection (with visualization) over a present situation.
<br>
• Combine activities – besides only designing, consider combine with researching and teaching. These activities will possibly enrich the design process.
<br>
• Transform realities – about how people live or how we use resources
<br>
• Evolve the craft – explore materialities

<br>
We were asked to further develop the definition of ‘who you are’ based on acknowledgements from the hybrid profiles we visited. As a form to cross my identity to the ones of the professionals we met, I found easier first to describe my profile.
<br>
<br>
 I am an architect that has been exploring digital fabrication tools to develop flexible, minimal and transmutable furniture for the contemporary needs of living. During this trajectory, I learned a good amount of carpentry knowledge and about CNC milling processes. I feel this is a knowledge that I would like to share with other people, mainly with other women, as I see the empowering potential it has. In addition, I would like to share with people in need to learn a new skill, people that live in underprivileged communities, as I see this knowledge has a transformative potential to create better living environments.
<br>
<br>
During this trajectory, I have got in touch with the concept of Open Design, a practice that interests me deeply. Along with my studio partner, we studied ways we could implement this idea to our business, but we have not reached a viable solution yet. Other topic that interest us to put in practice at the studio is the possibility of local production collaborating with local makers around the world. The possibility of sending a file instead of the product (that involves issues of packing and transportation) is an action with potentiality to change the way we consume and distribute things. Yet, these networks for partnerships are still much on their early stages, making it hard to be financially viable for both maker and designer. Since we could not establish a network for local making, we wanted to create awareness about the impacts of transportation and CO2 emission. Therefore, we created a CO2 emission calculator for people to acknowledge the impact of transportation. It was interesting to hear people’s views and interest about it, but the number of people that engages in environmental issues is still very low.
<br>
<br>
At the studio, we are also looking for materials and finishings in a responsible way, engaging in learning about their properties and impacts, as for example, VOCs. We want to communicate for the people the importance of this care, but still we see a barrier for people caring about it. Again, the environmental awareness of people is still low.
<br>
<br>
Objectively, points that define who I am, which have inspiration from the visited profiles and beyond are:
<br>

•	I am an architect shifting my interest from designing spaces and constructing them into thinking how spaces are lived and how people can adapt to changing scenarios in buildings and urban spaces.
<br>
•	I want to create awareness about choices that creates environmental impacts in both nature and the built space, resulting also in impact on people’s health.
<br>
•	After the visits, I realized that researching and sharing knowledge might be elements to create new dynamics around the practice of designing and this kind of alliance among activities started to interest me.

<br>
As for the points indicated, I achieved to change my practice with the creation of a studio focused on the topics of interest. I almost achieved to create the awareness for environmental issues, since some people engage but still in a small number. A point to be achieved is related to incorporating research and knowledge sharing in my practice.
<br>
<br>
Another topic we were asked to develop is related to our personal interest in the courses occurring during the first trimester. According to this, I believe that the ones that add to a personal development plan are:
<br>
<br>
*Already done:*
<br>
• Design for the real digital world: circular economy, materiality exploration, tools for fabrication.
<br>
• Exploring hybrid profiles: multiple fields of activity, new views over society and its habits.
<br>
• Navigating the uncertainty: questioning of the current state of things.
<br>
<br>
*To be done:*
<br>
As these courses are yet to come, from their descriptions I believe they will be useful for the commented reasons.
<br>
• The way things work: reframing conventional assumptions from objects and habits in society.
<br>
• Living with ideas: question your interests and explore them.
<br>
• Engaging narratives: learn how to communicate your ideas and engage other people in the topic.
<br>
<br>
*Extra activities*
<br>
In addition, we were asked to mention potential topics of interest for extra activities. From the visits we made, it outstand to me the fact that many designers combine activities, not only they are designing, but they are developing researches with engaged institutions and they are teaching and sharing their remarkable knowledge. Therefore, I believe a good addition to the master’s program would be to include:
<br>
• research methodologies
<br>
• teaching methodologies
<br>
<br>
### Vision and Identity classes
<br>
On the last two days of the week, we had classes with Dr. Oscar Tomico on the topic of *Vision and Identity – tools and techniques for self-reflection*. We discussed the impressions from the visits and how they were related to our own identities and trajectories. Then, we discussed about the master’s program and how the first trimester could influence our personal development. On the next day we read three articles related to the topics of vision, reflection and experience.
<br>
<br>
**Vision** - HUMMELS, Caroline. FRENS, Joep W. Designing for the unknown: a design process for the future generation of highly interactive systems and products. International Conference on Engineering and Product Design Education: Barcelona, 2008.
<br>
<br>
**Reflection** - LÖWGREN, Jonas. Annotated Portfolios and Other Forms of Intermediate-Level Knowledge. ACM Interactions: New York, 2013.
<br>
<br>
**Experience** - TOMICO, Oscar. WINTHAGEN, V. O. VAN HEIST, M. M. G. Designing for, with or within: 1st, 2nd and 3rd person points of view on designing for systems. NordiCHI: Copenhagen, 2012.
<br>
<br>
By the insights given from the papers, I could acknowledge some points to get a sense of direction for my future research project. A vision is much needed to integrate an approach to a topic, analyze, make prototypes, validate and transit among these stages with openness and flexibility. A reflection provides annotations, exemplars, guidelines, tools, methods and also critique. With an experience, you work among other people and their capabilities and can break barriers, you do not impose a vision, the direction is discussed in community with transparency and flexibility to be able to transform something in the real world. All the three visions give a sense of modesty in the proposition of a new system or idea, which must be discussed and validated with society to be able to make a real change.
<br>
<br>
### Vision
<br>
To conclude the week, we worked on a draft version of our vision, for the future, as a designer. First, what is a vision? As a group, we did a brainstorming of words that could define what a vision is. It was mentioned: a goal, a path to follow, a driving force, a scenario, authenticity, a framework, a coherent thought, an inspiration, a commitment. Starting from my identity, as an architect wanting to create awareness about societies’ choices that creates environmental impacts, I defined three topics to guide me as a vision for the future.
<br>

•	Know where I am (place and context)
<br>
•	Manipulate wisely the resources given (work in circularity as much as possible)
<br>
•	Generate other positive impacts (societal and environmental) with my design practice (sharing knowledge/information/resources)

<br>
With these three straightforward guidelines I can project ideas of how I will develop my future work as a designer in the next ten years at least. Moreover, they give me an assurance of who I am (linked to where I will be doing my practice and in which context it will fit) and why I will be doing the proposed work (to create awareness about societies’ choices and achieve a future change in habits and current practices that impact our environment).

---
title: 05 • Navigating the Uncertainty
period: 29 October - 02 November 2018
date: 2018-11-04 12:00:00
term: 1
published: true
---

*Navigating the Uncertainty* (29.10-05.11.2018) was a decisive week, providing us with critical discussions over the most urgent topics of our times. On a personal note, I have two remarks that add to the beginning and end of the week. Starting with the news of Brazilian’s election results of Jair Bolsonaro’s victory, the ultra-right candidate much pointed as a [threat to our planet’s environment](https://www.theguardian.com/commentisfree/2018/oct/24/planet-populists-brazil-jair-bolsonaro-environment). And ending with my experience of joining IAAC’s research trip to the Biennale de Architettura 2018, taking place in Venice, in a moment when the city experienced the high tide phenomenon.
<br>
<br>
{% figure caption: "*Venice, Italy. November 1st, 2018.*" %}
![]({{site.baseurl}}/w05_01.jpg)
{% endfigure %}
<br>
### Discussions on classes
<br>
The first discussion we had was with José Luis Vicente, who brought a critical approach to climate change. Thereafter, Pau Alsina brought a critical approach on society and technology. Tomas Diez and Mariana Quintero joined the discussions and on the last day, Quintero talked about interfaces and our human perception – how we interpret and sense the world and how limited we can be. To conclude the week, Dr. Oscar Tomico joined Mariana Quintero and Tomas Diez for a session of reflections over our interests that are to be developed in the research project.
<br>
<br>
José Luis de Vicente started his talk presenting the exhibition he curated at CCCB, “Després de la fi del Món”, that touches the topic of the psychological aspect of how we face the climate and social changes in the world. He talked about time scales and how nature has a timing very different from the capitalist system timing we are imposing on the world. We discussed about the Anthropocene and the impacts of the industrialization of the environment, as acceleration of extinction of species and pollution levels that governments are trying to fight back with the 2030 Agenda. The different points of view of the current state of things were discussed: Decreationists, Capitalists, Accelerationists, Extinctionists and Mutualists. The discussion ended with reflections over ‘how we, as designers, help to shift perceptions to change habits in society?’
<br>
<br>

Connected much to this point over the power of decisions in society was the talk with Pau Alsina, in which we discussed the article “Unfolding the Political Capacities of Design” wrote by Fernando Domínguez Rubio & Uriel Fogué. We first navigated the meanings of society, what is design in society and if does design produces society. In discussing the political capacities of design, we explored the concept of the ability to transform society through design. The presence of relationships of power in things and ideas from Michel Foucault were explored deeper. When talking about how all design is social and political, I found really interesting the discussion about micro politics and how we can engage people in noticing these impacts and to change habits towards more conscientious ones. Every decision people make is a political decision and it has an impact in the systems we live. How can we, as designers, help society acknowledge the power of their decision?
<br>
<br>
It is a real challenge to communicate and engage people in acknowledging the political power of their decisions and in times that machines are being designed to make decisions for us, we must need to be aware of the implications this have in democracy. While exploring more about the topic of micro politics and decisions impacts, I found this interesting article concerning these matters:
<br>
<br>
SPRENGER, Florian. The politics of micro-decisions, or: visions for the democratic control of movement. 23 May 2016. Available [online](https://www.opendemocracy.net/digitaliberties/florian-sprenger/politics-of-micro-decisions-or-visions-for-democratic-control-of-movement).
<br>
<br>
The discussion with Mariana Quintero about interfaces and our comprehensions and limitations to these understandings was a great closure to the topics discussed during the week. Discussions on consciousness and the perception of symbols and how we communicate through them were much valuable for me afterwards. It all connected for me with the discussion about design and its political force, generating dynamics over social and environmental systems and helped me conceptualize my idea for the research project.
<br>
<br>
Quintero also recommended us to read the essay [*Leverage Points: Places to Intervene in a System* by Dr. Donella Meadows](http://donellameadows.org/archives/leverage-points-places-to-intervene-in-a-system/). It was a very inspiring read since Dr. Meadows approach issues with a holistic point of view.
<br>
<br>
“When you understand the power of system self-organization, you begin to understand why biologists worship biodiversity even more than economists worship technology. The wildly varied stock of DNA, evolved and accumulated over billions of years, is the source of evolutionary potential, just as science libraries and labs and universities where scientists are trained are the source of technological potential. Allowing species to go extinct is a systems crime, just as randomly eliminating all copies of particular science journals, or particular kinds of scientists, would be.”
<br>
<br>
“Paradigms are the sources of systems. From them, from shared social agreements about the nature of reality, come system goals and information flows, feedbacks, stocks, flows and everything else about systems. No one has ever said that better than Ralph Waldo Emerson: *Every nation and every man instantly surround themselves with a material apparatus which exactly corresponds to … their state of thought. Observe how every truth and every error, each a thought of some man’s mind, clothes itself with societies, houses, cities, language, ceremonies, newspapers. Observe the ideas of the present day … see how timber, brick, lime, and stone have flown into convenient shape, obedient to the master idea reigning in the minds of many persons…. It follows, of course, that the least enlargement of ideas … would cause the most striking changes of external things.*”
<br>
<br>
### Research Project Idea
<br>
I have been trying to work over issues of sustainability in the architecture/interior design field with not much success of communicating this to people. In addition, topics relating to consumption have started to interest me more over the last year. Therefore, for the research project I had the insight to overlook another aspect on the society, the topic of rituals and how they have become more and more materialized and inorganic. I want to study the evolution of rituals in different societies and try to identify a path to regenerate them into more organic practices.
Keywords for the research are: Anthropology, Consumerism, Climate Change, Human Impact, Anthropocene, Circularity, Bio/New Materials.
<br>
<br>
In regarding to a reading list, I will start first understanding the different definitions of rituals and how consumerism is attached to the practices.
<br>
<br>
• DE COPPET, Daniel. Understanding Rituals. London: Routledge, 1992.
<br>
• ETZIONI, Amitai. BLOOM, Jared. We Are What We Celebrate – Understanding Holidays and Rituals. New York: New York University Press, 2004.
<br>
• OTNES, Cele C. LOWREY, Tina M. Contemporary Consumption Rituals – A Research Anthology. Mahwah: Lawrence Erlbaum Associates Inc. Publishers, 2004.
<br>
• PILS, Giulia. TROCCHIANESI, Raffaella. Design and Ritual. Crossed Narratives Among Design, Anthropology and Sociology. Paris: 11th European Academy of Design Conference, 2015.

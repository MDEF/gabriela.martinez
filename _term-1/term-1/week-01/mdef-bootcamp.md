---
title: 01 • MDEF Bootcamp
period: 02-05 October 2018
date: 2018-10-07 12:00:00
term: 1
published: true
---


*MDEF Bootcamp* (02-05.10.2018) was an introductory workshop for students to understand the structure of the master program, get to know better their colleagues and understand the context in which they are now inserted for the next year. In this sense, it included an exploratory walk around the Poblenou district, which will be described later.
<br>
<br>
### First Discussions
<br>
<br>
On the first day, each student had to talk about their background and interest in order to form groups of interest. We were asked:
<br>
<br>
• Who are you and why are you here?
<br>
<br>
• How do you see yourself in 10 years?
<br>
<br>
On the second day, we were presented to the tools that will help us document and share the process of our development during the course. The platform GitLab and the code editor Atom will be the main tools of work. Mariana Quintero presented to us the concept of Git, which is a version-control system for tracking changes in computer files and coordination work among files. She also exposed the importance of recording the processes and of finding your own ways of systematization. Human interactions, human – machine interactions and machine interactions were also part of the talk, giving us an interesting insight about these processes and protocols of information exchange.
<br>
<br>
![]({{site.baseurl}}/w01_02_atom.jpg)
<br>
<br>
The third day was very dynamic, we did the “City Safari” around Poblenou and later collected materials on the streets for our next projects. There is a culture in the city to share furniture and other objects which people don’t want anymore in the streets during a specific day of the week. Each neighborhood has its own day to share things and this information can be found through its popular name of “Día de los Trastos”.
<br>
<br>
Previously to these activities, Tomas Diez presented us his trajectory, detailing the projects in which he has worked and is currently working. They are related to his interest on the urban dynamics, about how things are produced and distributed and how people interact with them. The main concept of the Fab Lab was explained and specific projects highlighted: Fab Lab Mobile, Fab Lab House, Fab Textiles, among others. Diez also showed us Smart Citizen and Fab City, two projects that go deep in understanding current patterns of society and how to make a change on them.
<br>
<br>
### City Safari
<br>
<br>
During the “City Safari” we came to know some incredible initiatives and companies.
<br>
<br>
[Leka restaurant]( http://www.restauranteleka.com/) is an open-source restaurant. That means that all the furniture of the space as well as the food is available through their website for people. You can find the recipes and the instructions for production of furniture and do it by yourself. It is interesting to see a business model that extrapolates its purpose (of a restaurant to serve food) and brings new interesting interactions to its clients (proposing to make their furniture or try the recipes at home).
<br>
<br>
{% figure caption: "*Leka . restaurant space*" %}
![]({{site.baseurl}}/w01_03_leka_01.jpg)
{% endfigure %}

{% figure caption: "*Leka . QR Code for information on furniture*" %}
![]({{site.baseurl}}/w01_03_leka_02.jpg)
{% endfigure %}
<br>
<br>
[Indissoluble](http://indissoluble.com/fs/) is a company focused on developing temporary structures for exhibitions and fairs. The refinement of their work is impressive and the incorporation of technology in the process making as well as in the final product is really interesting.
<br>
<br>
{% figure caption: "*Indissoluble . working tools [patterns of organization]*" %}
![]({{site.baseurl}}/w01_03_indissoluble.jpg)
{% endfigure %}
<br>
<br>
[TransfoLAB BCN]( https://www.transfolabbcn.com/) is a hub to help people learn and create from recycling materials. The space is compound of a showroom, office, workshop and laboratory. Nada, one of the founders, explained to us that they also do courses to engage people in learning about recycling and circular economy. Mostly of their furniture was done by the transformation of other existing objects.
<br>
<br>
{% figure caption: "*TransfoLAB . office*" %}
![]({{site.baseurl}}/w01_03_transfolab_01.jpg)
{% endfigure %}

{% figure caption: "*TransfoLAB . office*" %}
![]({{site.baseurl}}/w01_03_transfolab_02.jpg)
{% endfigure %}

{% figure caption: "*TransfoLAB . Talk with Nada and MDEF students*" %}
![]({{site.baseurl}}/w01_03_transfolab_03.jpg)
{% endfigure %}

{% figure caption: "*TransfoLAB . working tools [patterns of organization]*" %}
![]({{site.baseurl}}/w01_03_transfolab_04.jpg)
{% endfigure %}
<br>
<br>
We continued our exploration of the neighborhood going to the Superilla project. A very interesting idea to bring people to really use the streets and by that, also reduce the passage of cars, which normally bring pollution and noise. Some flaws on the implementation of the project brought resistance from residents and companies at the area, for example, car dealer’s stores hanging posters against the project.
<br>
<br>
{% figure caption: "*Superilla . people on the street*" %}
![]({{site.baseurl}}/w01_03_superilla_01.jpg)
{% endfigure %}

{% figure caption: "*Superilla . opposition to the project*" %}
![]({{site.baseurl}}/w01_03_superilla_02.jpg)
{% endfigure %}

{% figure caption: "*Superilla . interaction among cars and people at the area*" %}
![]({{site.baseurl}}/w01_03_superilla_03.jpg)
{% endfigure %}
<br>
<br>
Later, we passed by a plot intended to be an urban park for people, but it was empty and it seemed not much an attractive area to stay, in my opinion. On the other side of the street, a little passage caught my attention more than the park. This led to the reflection of “What makes an urban space attractive to people?”  and “What make people care and engage with urban areas?”.
<br>
<br>
{% figure caption: "*Poblenou . urban park*" %}
![]({{site.baseurl}}/w01_03_plot_01.jpg)
{% endfigure %}

{% figure caption: "*Poblenou . urban passage*" %}
![]({{site.baseurl}}/w01_03_plot_02.jpg)
{% endfigure %}
<br>
<br>
Also, nearby, a pattern painted on a wall and a door caught my attention by its composition, then, to just later notice an interesting pattern of human behavior over it: some graffiti where painted only on the door. What restrains people? Why do we follow other’s inexplicably patterns? It seemed a curious contradiction that graffiti, which intend to break rules, were there following an underlying “rule” (or better, a pattern or process).
<br>
<br>
{% figure caption: "*Poblenou . facade and grafitti*" %}
![]({{site.baseurl}}/w01_03_plot_03.jpg)
{% endfigure %}
<br>
<br>
We then finished our exploration visiting the [Biciclot initiative]( http://www.biciclot.coop/), a social project devoted to teach the mechanics of bicycle maintenance to people and in schools. They also reassemble bicycles from parts that the municipality collects from the city. These new bicycles are given to people in need and to projects related to kids. The space is also open for people to come and work on reparation of their own bicycles.
<br>
<br>
{% figure caption: "*Biciclot . visit*" %}
![]({{site.baseurl}}/w01_03_biciclot_01.jpg)
{% endfigure %}

{% figure caption: "*Biciclot . kid working on a bicycle*" %}
![]({{site.baseurl}}/w01_03_biciclot_02.jpg)
{% endfigure %}

{% figure caption: "*Biciclot . working tools [patterns of organization] / working and listening to “The Very Best of Curtis Mayfield”*" %}
![]({{site.baseurl}}/w01_03_biciclot_03.jpg)
{% endfigure %}
<br>
<br>
<iframe src="https://open.spotify.com/embed/track/5wdlG60d0WHoo8P3QzrlbG" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
<br>
<br>
After the “City Safari” we started to walk the streets of Poblenou in search of materials for the next projects. It was an interesting shift to start looking for things in the city that I normally don’t look at. In addition, it was interesting to see people noticing us while collecting things and in some situations, being kind, like, people or motorcycles stopping for us to pass. Do they notice or be kind to the usual material collectors?
<br>
<br>
On the last day of the MDEF Bootcamp, Oscar Tomico presented part of his work on *Intimate, Situated, Embodied and Soft Wearables*. He highlighted his interest in how textiles can allow to naturalize technology, but also these two materials have physical differences. By that, he pointed the need to change approaches to design out of the comfort zone and to be open for new results. Furthermore, he emphasized the importance of feeling and experiencing a design or idea before it reaches the user.
<br>
<br>
 Later, the groups exposed their reflections over previous assumptions, impressions, new findings and reflections about the week. It was an enriching moment to listen and acknowledge other perspectives of the experience. We discussed a lot about the Poblenou neighborhood and its dynamics. This discussion led me to reflect about the impact of time in spaces and processes. For how long Poblenou will be what it is? Will the new companies that are arriving to the neighborhood change its land use dynamics and evict current users and residents? Will the small and exciting new businesses achieve to survive? For how long people that are marginalized will keep on doing the informal job to collect materials on the streets for survival? Reflections that may lead to a better understanding of the impact of time in the society and the territory and bring further discussions on how to change current troubled processes.
 <br>
 <br>
![]({{site.baseurl}}/w01_04_map.jpg)
<br>
<br>
### Conclusions
<br>
<br>
In an objective way, this week learning process was based in acknowledging previously unknown information (both virtual and physical) to develop the working process:
<br>
<br>
•	Learn new work tools: GitLab and Atom to create a free and highly customizable website.
<br>
• Visualize potential on society dynamics of exchange: The informal yet formal dynamics of the “Día de los Trastos” can provide sources for future projects.
<br>
•	Reframing the creative process of design: Work/creation can be done through transformation. Not only the act of drawing/thinking an idea can lead to creation. Creation can be achieved out of the transformation of the available elements that surround us.
<br>
•	Reframing the sources for production: Visualize that the city and existing elements are also a source for working material, not only raw materials can offer us content for new creations.
<br>
•	Observe and question the relationship of human beings with the built environment: Perception of patterns of the human behavior in Poblenou and how the configuration of spaces can condition people to use it, reject it or contest it.
<br>
• Reflections on the duration of the current urban dynamics: The time factor over people (users and residents), over projects and processes (the occupation of the existing buildings) and over the territory (the materiality of the existing buildings and the urban configuration).
<br>
<br>
The overall experience of the week brought as a relevant aspect to my work the understanding of current processes in the context of the local dynamics. This will be the base for an exploratory investigation over how we can achieve to change patterns and processes that no longer should be perpetuated.

---
title: 09 • Engaging Narratives
period: 26-30 November 2018
date: 2018-12-02 12:00:00
term: 1
published: true
---

<br>
<br>
*Engaging Narratives* (26-30.11.2018) focused on presenting us the elements to turn our area of interest in a captivating story. The week started with the guest tutor, Heather Corcoran, who works with design and technology at Kickstarter. She comes from a background in the art field and fundraising. She shared with us some fundamental points of telling your story, in which you have to share your vision and reach a community. Later, she presented some successful campaigns made with support from Kickstarter and went through the main points for making a good campaign: video, description, reward and updates.
<br>
<br>
We did a quick exercise of storytelling, which consisted to write a narrative based on our area of interest. Still keeping the field of the ways society has been deploying rituals, my result was:
<br>
<br>
Title: Celebrate ephemerally, a tools set
<br>
<br>
Subtitle: Our festive moments are momentary and we are creating loads of trash from it. What if we could celebrate it in an ephemeral way with the same enjoyment? *Celebrate ephemerally* is a set of tools to reframe our rituals.
<br>
<br>
As feedback from Corcoran, she said my language was straightforward and clear, yet, I should focus on explore what kind of output from this interest I would have. “How can you narrow down the intangible?” she stated. She suggested to explore the idea of a game, a recipe book, a graphic design poster or an event (so information could be passed from people to people). She believes that I should be careful with the use of the word ephemeral: since that any kind of product, even if they are made of more ecofriendly materials like bioplastics, are not ephemeral. She suggested to focus on gestures or performed actions on the basis of rituals. Although I find this output really interesting, I started to question my intentions in diving into this topic in a more abstract way.
<br>
<br>
These questionings were useful to reframe my interests and even question the reason why I chose to be doing a master at IAAC. Corcoran shared with us the article [*A creative person’s guide to thoughtful promotion*](https://thecreativeindependent.com/guides/a-creative-persons-guide-to-thoughtful-promotion/) and the main and start point on it was to know your goals. This resonated with me during the week as I was questioning the direction of my research interest.
<br>
<br>
The other guest tutor in this week was Bjarke Calvin, founder of a new social app. Calvin has a background in documentaries and digital content. He shared with us a presentation on the importance of documentaries as tools for storytelling. Moreover, talked about how social media as it is now is not serving society as a beneficial tool. He is looking for the next step after social media, so that is why he developed a new app, Duckling. We did a small exercise in creating a story on it, related to a lesson learned from a project positively or negatively. All stories were tagged with “emergent futures”.
<br>
<br>
<iframe src="https://duckling.me/gabriela.mpsg/5784652306424" height="320" width="500" allowfullscreen="true"></iframe>
<br>
<br>
For the closure of the week, we needed to present a visual trajectory of what our fields of interest has taken us so far. Since I was questioning my interests during this week, I felt I needed to reframe my direction, and decided to present a timeline that would tell this story. I added all the points absorbed during the weekly exploration in MDEF and my reflections during this time. From looking at rituals, I reframed my interest in developing a project related to local connection and making. From that, I started to think in a path that, by looking at rituals and bringing them to the environment of Fab Labs, I could promote community reconnection and literacy in digital fabrication and materials. Sustainability, materiality, making and connection would be the guiding words of this trajectory.
<br>
<br>
{% figure caption: "*timeline of area of interest*" %}
![]({{site.baseurl}}/w09_01.jpg)
{% endfigure %}
<br>
<br>
{% figure caption: "*guide words*" %}
![]({{site.baseurl}}/w09_02.jpg)
{% endfigure %}

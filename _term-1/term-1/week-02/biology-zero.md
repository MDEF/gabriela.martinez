---
title: 02 • Biology Zero
period: 08-12 October 2018
date: 2018-10-14 12:00:00
term: 1
published: true
---

Life is made of matter and energy. Nature uses energy to combine elements into molecules, then macromolecules that will group and form cells, tissues, organs, to then, complex systems of living beings. Humans are these complex systems that developed consciousness, being able to acquire knowledge and become actors around processes - one of them being biology. Our species devote its time into transforming the environment to make our lives better, easier, more beautiful. We are makers. However, our unthoughtful interventions on nature are going too far, going towards a point that the impact might be non-reversible. Biological processes might inspire and help us find ways to be more resilient and regenerative in our activities.
<br>
<br>
*Biology Zero* (08-12.10.2018) was an intense week, in which we were exposed to basic concepts of biology and introduced to biochemistry, metabolism, microbiology, microbiomes, cellular biology, histology and synthetic biology. As I reviewed my notes, I decided to make a word cloud diagram to connect all topics discussed during the week with these two principles of life: **matter** and **energy**.

![]({{site.baseurl}}/w02_word-diagram.jpg)

During the week, we were also asked to think about a designed experiment, create a hypothesis and use the scientific method to propose a solution.
<br>
<br>
### Hypothesis
<br>
[Recent studies](https://www.nature.com/articles/nature24672) have concluded that there is a 93% chance that global warming will exceed 4°C by the end of this century if we keep the pace of production and consumption of our current times. Changes of temperature will affect us in unpredictable ways in the future. In addition, it maybe can affect living beings that are essentially necessary for us, like the ones in our gut microbiome. Some scientists are already addressing this concern, as seen in [a research](https://www.nature.com/articles/s41559-017-0161) over the correlation of rising temperatures and the gut microbiome of lizards. It was concluded that a rise of 2-3°C caused a 34% loss of microorganisms diversity in their guts. In addition, the beneficial ones were the first to die. With this scenario in mind, I addressed as a hypothesis:
<br>
<br>
**If a 4°C increase of temperature happens by the end of the century and degrades our gut microbiomes, will an adapted gut microbiome be required for human survival?**
<br>
<br>
### Methodology
<br>
A way to confirm this hypothesis would be to develop or look for an existing database of human gut microbiome and compare the gut microbiome of people that live in hotter places to the ones from people living in colder places. In this comparison, it would be needed to search for different microorganisms and study their different roles in our gut. Then, it would be possible to find more adapted microorganisms to the rising temperatures. These specimens could then be used for a designed evolved gut microbiome, which could be turned into supplements for human beings.

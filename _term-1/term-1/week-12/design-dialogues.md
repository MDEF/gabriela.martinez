---
title: 12 • Design Dialogues
period: 17-19 December 2018
date: 2018-12-23 12:00:00
term: 1
published: true
---

<br>
<br>
*Design Dialogues* (17-19.12.2018) was the concluding week of the first trimester of the master. A presentation of the deployment into our areas of interest was set and a conversation with tutors and guests took place.
<br>
<br>
My area of interest is in the field of making and creating bonds between people and nature in this process. I defined that I will work on a research of **local connection through making in the context of the redistributed manufacturing world**.
<br>
<br>
{% figure caption: "*Overview of my presentation*" %}
![]({{site.baseurl}}/w12_01.jpg)
{% endfigure %}
<br>
<br>
To approach the idea of local connection, I decided to map how I have been consuming, to understand my choices related to my belongings and consumables in the context of where they were made. I decided to read the labels of the products and categorize them, prioritizing the composition, where they were made and where I have acquired them. Reading the labels gave me many interesting insights, mostly related to the lack of transparency in the information about where things are made. In addition, it was important to be aware that this is only a layer in the whole system of production, since labels normally do not tell us where the materials and inputs come from. During the *Design Dialogues*, I presented two samples from this mapping. Two soaps that have different approaches in their making, from the composition and production process to the way they are packaged and delivered.
<br>
<br>
{% figure caption: "*Mapping and samples of products*" %}
![]({{site.baseurl}}/w12_02.jpg)
{% endfigure %}
<br>
<br>
My questioning in the whole process is that we need to visualize that design is not only an image or final product, it is a process, it has expertise, cultural history embedded and it is materiality. In our current model of consumption, we are missing the connection with the making, since we are distancing ourselves from the act of buying in the globalized world. I have been asking myself how we can collectively understand design and its role in our urban lives. I think that one path to do so is through local connection. With people through identities, shared knowledge, patterns, shapes. With nature through materials, circularity and technology. Therefore, in the next trimesters of the master, I want explore this possibilities in the local context.
<br>
<br>
There are some possibilities already known in this field of interest, as I showed in my references video, with articles and projects that create this link and bring awareness to people about topics related to our consumption behavior. Here I indicate few of them, that were of great inspiration:
<br>
<br>
• [“Algae lab” by Atelier Luma – new material based on local algae for 3D printing]( https://atelier-luma.org/en/projects/algae-lab)
<br>
<br>
• [“A mão do povo brasileiro” exhibition by Lina Bo Bardi – a research in the popular material expression of Brazilian people]( https://www.galleriesnow.net/shows/the-hand-of-the-brazilian-people-1969-2016/)
<br>
<br>
• [“In a Grain of Sand” by AtelierNL – collaborative research about sands from around the world]( http://www.ateliernl.com/projects/in-a-grain-of-sand)
<br>
<br>
• [Superlocal – making products with the local availability of materials]( https://www.super-local.org/)
<br>
<br>
• [Christien Meindertsma – designer with projects relating production and consumption](https://christienmeindertsma.com/)
<br>
<br>
Moreover, in the context of researching local and new materials, bioplastics have risen as a topic of interest. It was really nice to know that some people are researching this at IAAC and to get to know their projects and ambitions. I have a lot to thank for Barbara Sanchez who lent me some samples of bioplastics to be shown in my presentation.
<br>
<br>
As the conversation started with tutors and guests, I had the opportunity to talk with many of them but not all of them. I will comment the most important topics of their feedbacks below:
<br>
<br>
**Elisabet Roselló and Lucas Peña**
<br>
<br>
Lucas said I did a good overview about the topic, but I should work on creating in depth approaches to link production with people. He said I have a lot of space to grow forward. Elisabeth asked me what kind of people I would be targeting with my project and that I should look for what kind of path I would be taking to develop it more.
<br>
<br>
**Chiara Dall’Olio**
<br>
<br>
Chiara suggested that a path I could choose to develop my project could be the creation of a toolkit. The toolkit could orient local communities in this process of local connection through making. She then shared with me her experience of being part of the Making Sense project, which involved three cities in Europe. Each city had its own sensor’s kit to develop the interaction with citizens. The working team explored how to empower local community through local technology to solve environmental problems.
<br>
<br>
**Tomas Diez and Markel Cormenzana**
<br>
<br>
Tomas commented that my project focus on quantifying the things that we do not want to see. To make visible the hidden information, which is quite obvious that we cannot keep living the life that we are all living. He commented that, in my exploration of local connection through making, there is a step before that I need to understand. This is about our material disconnection and its implications for the future, our living in the future.
<br>
<br>
Markel highlighted the interesting connection that exists between digital fabrication and locally making, but also pointed out that most fab labs works with “universal” materials. So, this exploration of local materials and local knowledge is a really interesting topic to work on. He stated: “Designers are mediators of materiality, so how does this is connected to digital fabrication? How different is digital fabrication in Barcelona from the one done in Singapore? And how does this connect with the way we consume?”
<br>
<br>
Tomas raised the question of what materials we really can replace or how do we reduce the amount of kilometers that we carry on top of ourselves. Because also, the kilometers involved in our products are relying on fossil fuels and extractive economy. He highlighted that the self-assessment I started could be done in a deeper way, of understanding my possessions or even include class colleagues in this mapping, and from that, it would come out many other possible areas of intervention.
<br>
<br>
Markel mentioned that connected to materiality, all objects come with values and histories, so I could map the things I own and which values and histories are encapsulated by these materials also. Therefore, I should think of how I could connect the different narratives. Tomas and Markel commented about the [Provenance project]( https://www.provenance.org/).
<br>
<br>
Tomas commented that the topic I was raising about material awareness would be a super interesting field to dive in. Markel then highlighted that an interesting area to look for would be the packaging. In material awareness, packaging is getting to be a hyper-object, it is everywhere and we do not have the awareness of it. It would also be a way to raise awareness to the whole system involved to bring a product to a consumer.
<br>
<br>
Tomas concluded that my area of interest was clear but I should dive deeper in my data and extract significant information to refine my area of intervention.
<br>
<br>
**Ingi Freyr**
<br>
<br>
Ingi commented that would be interesting to go through my list of belongings and try to see if I could source them locally, to then visualize the differences they would have. Compare my original belonging and the one made locally. He also mentioned that he believes in the power of technology, since new technology would made possible to create more things in a smaller area. He also mentioned that my project could enable to bring back economic activities to areas that had some expertise – like Catalonia and France that used to be strong textiles producers.  
<br>
<br>
**Jonathan Minchin and James Tooze**
<br>
<br>
James mentioned to me about a project he launched last year, “Design for local variables”, that deals with distributed manufacturing and how locality changes it parameters. He commented that the challenge it is not about de-globalizing, but about re-globalizing the world. He added that we can have globalized production but we need to think about local variables.
<br>
<br>
Jonathan suggested that I should refine the parameters of my path and look for the metabolic pathway (of material conversion). I should then identify clearly the parameters that I am dealing with and not, and the pathways of conversion within those parameters.
<br>
<br>
James questioned my use of the word local, since it could be hyper-local or regionally local. Then, I should reflect on what I mean by saying local. He suggested that an interesting path could be to find example objects that could be the right kind of objects to be made locally (maybe clothes or utensils) so to re-globalize the approach to make those things. I should then describe the objects, materially and about its process, so I could think about where I am and how local factors can exist in that product. It would be an interesting piece of analysis between the relationship of things, materials, infrastructure and local variables about recapturing materials.
<br>
<br>
James also gave a great insight on how to start creating craft not only in the product but also into the process by looking at culturally where it is from and by so, maintaining the sensibility of doing in place – a thing we miss when production is centralized. Apply the craft to the economy industry, than you will meet at scale, which is able to disrupt something that is happening elsewhere. Jonathan concluded by saying that I should also look historically how things are made and search for the tools that make things.
<br>
<br>
### Conclusions
<br>
<br>
Tutors and guests comments gave me many interesting insights and ideas of how to conduct my research from now on. I visualize that I have many possible paths to follow. Now I need to decide which route I will take to make a more consistent statement in my area of interest and consolidate my area of intervention.
<br>
<br>
In previous conversations with tutor Mariana Quintero, she gave me ideas for three possible paths to follow with my project. It would be to deploy it in art, design or a methodology. In these three possible scenarios, I believe that either design or methodology are the ones that appeal more to me. I think that a design process or a methodology to engage people are the best tools to create the awareness about our current complex consumption patterns and establish a better connection between people and nature in the local context. I would rather envision a product that express the process of a deeper connection to a place or a methodology to involve citizens and/or children in creating a better relationship with their locality. 

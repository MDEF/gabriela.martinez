---
title: 07 • The Way Things Work
period: 12-16 November 2018
date: 2018-11-25 12:00:00
term: 1
published: true
---

<br>
<br>
*The Way things work* (12-16.11.2018) was a mix of lectures and hands-on week, with theory about physical computing, electronics and networks but also a lot of practical work, learning from real objects and making machines. Guillem Camprodon and Víctor Barberán were the tutors, with assistance of Oscar González.
<br>
<br>
The first lecture presented us the importance of moving forward into thinking about systems instead of designing gadgets that are only attached to networks. It was also highlighted the importance of being critical over the reason we design objects and connect them, as so we can make things that really actuate in the world with a purpose.
<br>
<br>
Then, we had a hands-on activity: we were given some devices (different types of printers and a coffee machine) to be dissembled and learn about the inner parts that compound them and make them work. It was really interesting to get to know both the mechanical design and digital design of these devices. We discussed about materials, the use of different types of plastics, how they are injected into shapes, the fiberglass and copper conduits in boards and specific parts, like a metal part that control the heating in the coffee machine or pieces of fiber that absorb the excess of paint inside the printers. It was also interesting to learn how the designs are done in a closed way, like boards that control printer cartridges so they cannot be endlessly refilled and how these devices have microcontrollers with encrypted codes to avoid being hacked. During the week, it has become clear, for different reasons, the relevance of the open source in design and coding. Whenever you open your process and let other people learn from it and possibly improve it, society benefits from these improvements in design and making. Linux was a clear example of this: an open-source operating system developed and constantly improved by a community. Moreover, it was a surprise to hear that most big companies in tech and banking systems use Linux as their systems. The collective power of creating something consistent and in constant evolution is something that designers must incorporate more and more in the systems they create.
<br>
<br>
{% figure caption: "*Parts of a multifunctional printer*" %}
![]({{site.baseurl}}/w07_01.jpg)
{% endfigure %}
<br>
<br>
Also, we were introduced to the basic concepts of electricity: currents (AC and DC), voltage and resistance. Later we had a lecture of *How to make a computer*, in which we saw the evolution of the processing machines and the core principles that define the type of machine to be developed: analog x digital; binary x decimal; electronical x electromechanical; general x special purpose. We had a closer look on the general aspect of computers, the softwares and compiling (turning language into machine language). We learned the concept of PCBs and Arduino was presented in bigger detail.
<br>
<br>
On the next day, we did several experiments on Arduino to learn different concepts and learned that the best way to develop our projects would always start with drawing a flowchart. Making this scheme, we can always have a clear picture of the start and end; the inputs and outputs; and where processes and decisions happen. Then, we learned about the different elements we can attach to a microcontroller and develop the interactions and measurements we intend to do. We were presented the different types of sensors, actuators, sound devices, movement devices (motors) and pneumatic actuators. In addition, we watched videos that show how these pieces are used in the most innovative ways.
<br>
<br>
{% figure caption: "*Experiments with Arduino*" %}
![]({{site.baseurl}}/w07_02.jpg)
{% endfigure %}
<br>
<br>
Then, we started to brainstorm on our group project. My group decided to focus on deploying an output. Our premise was to have fun with the project and not necessarily develop something functional; we should have fun with the process and with the machine created. Many ideas involving sound and light were discussed, but in the end, we decided to develop a machine to explode balloons (and we had so much fun doing it!).
<br>
<br>
{% figure caption: "*Arduino connected to a relay – a piece that can control the air pump*" %}
![]({{site.baseurl}}/w07_03.jpg)
{% endfigure %}
<br>
<br>
Next day lecture was about networks, how information started to be transmitted on wires. The whole system was presented, from the way companies provide the service to internet protocols (IP), bandwidth, peer to peer connection and languages. An interesting discussion was about how networks are centralized but connected through many ways yet, content is not distributed, so the content in servers can be lost if a company decides to shut it down. Discussions around the future of internet should be addressing ways to move forward on this topic and think of content being fully distributed – a possible way should be stored by society in general, a creation of peer to peer systems.
<br>
<br>
Later, we did a simple activity to understand how language in a network works, by encoding an information. The exercise consisted of drawing a point in a piece of paper and then folding the paper in multiple pieces, with the concept that, whenever on the left side, it would correspond to 0 and whenever on the right side, to 1. We then could generate a sequence with 0s and 1s that would inform the position of the point and our colleagues could find out its position.
<br>
<br>
{% figure caption: "*How to encode information and share it*" %}
![]({{site.baseurl}}/w07_04.jpg)
{% endfigure %}
<br>
<br>
After that, we learned how to run a network in the class, so then for the final day we could connect our projects of inputs and outputs through this network. The network in the class would run on MQTT, an efficient protocol to communicate machines. We learned its principles and how to connect the ESP8266 board, a microcontroller that is embedded with wi-fi. Having set the network, we were able to send messages to the MQTT broker, the main server.
<br>
<br>
{% figure caption: "*Messages received by the MQTT broker*" %}
![]({{site.baseurl}}/w07_05.jpg)
{% endfigure %}
<br>
<br>
The activities of the week deployed in developing the final project. Our first prototype had a vacuum pump connected to a 2-channel relay (a piece that works like a switch) that connected to the Arduino board. Our first code was able to open and close the relay and provide the pump with energy to pump out air.
<br>
<br>
{% figure caption: "*First prototype of the machine to explode balloons*" %}
![]({{site.baseurl}}/w07_06.jpg)
{% endfigure %}
<br>
<br>
Then, we worked on the code to make it inflate a balloon. Our first test exploding a balloon was a success, yet it took so long to fill it till exploding that we decided to set up the machine with an air compressor.
<br>
<br>
{% figure caption: "*First test with a balloon*" %}
![]({{site.baseurl}}/w07_07.jpg)
{% endfigure %}
<br>
<br>
<iframe width="672" height="378" src="https://www.youtube.com/embed/7IubXally7Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<br>
<br>
We also wanted to incorporate lights in our machine, so we started to research how to code a led strip that would correspond its colors to the balloon getting bigger. It would start with blue lights, evolve to light blue, green, yellow, orange to finish with red when the balloon would be near to explode.
<br>
<br>
![]({{site.baseurl}}/w07_09.jpg)
<br>
<br>
We did another test with a compressor filling the balloon with air and decided we would go on with this system.
<br>
<br>
{% figure caption: "*Balloon being filled with air pumped by a compressor*" %}
![]({{site.baseurl}}/w07_10.jpg)
{% endfigure %}
<br>
<br>
On the last day, we created a scenario for the machine, where the balloon would be inflated from the top, fixated in a mirrored board attached to a beam. Moreover, we worked on refining the code to orchestrate the air pumping with the lights and the network communication. The help of the tutors was extremely important. We connected our output to the network and could link it to other group’s input through the Node.RED, which is a visual parameter tool to connect things and create sequences of processes.
<br>
<br>
{% figure caption: "*Node.RED interface*" %}
![]({{site.baseurl}}/w07_11.jpg)
{% endfigure %}
<br>
<br>
The exploding balloon output was linked with an input corresponding to a sensor that would perceive the entrance of a person through the class door. Every time someone passed the door, it would trigger the sequence of inflating the balloon. The final result can be seen in the video below.
<br>
<br>
<iframe width="672" height="378" src="https://www.youtube.com/embed/dOk9noK4XM4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<br>
<br>
### Conclusions
<br>
<br>
*The way things work* was a week that certainly, on a personal aspect, demystified the universe of electronics. It was really interesting to transit between theory of how things are set and done and actually see it materialized, get to know the design of parts that compound a whole product or machine. The discussion about closed design and controlled systems brought reflections about how could things be different if they were open to be developed and evolved by anyone.The society would benefit immensely. In this sense, it was amazing to be able to build our own machine and see it work. As well as, it was impressive to see the knowledge of the tutors and their kindness and willingness to always help us.

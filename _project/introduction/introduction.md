---
title: 05 • Introduction
period: 14 June 2019
date: 2019-06-14 01:00:00
term: 3
published: true
---


<br>
<br>
**Context**
<br>
<br>
In the hyperconnected world we live in, multiple and rich connections arise, but there is also a great tendency of losing connection to our real environments. Nobody really belongs to anywhere anymore, we are under constant global influence, and so our relationship to things. We buy goods made in faraway lands, without knowledge of material’s origins and the labor’s conditions in which they were manufactured. Moreover, the things we are buying have less and less relation to our context and cultural expressions. How to reconnect people to the trajectory of the making of a good? How to give consciousness of its individual impact or to the scale of linked impacts? Even if they have consciousness, how to engage people in making a real change on their consumption habits to choose wisely and go towards a circular chain?
<br>
<br>
These are the questions accompanying me since the very beginning of the master. I started exploring these topics by doing a self-assessment exercise in mapping the origins of all the belongings I brought to Barcelona for this year. It was an interesting exercise to notice patterns of origins, for instance, most of my clothing being made in Asia. Moreover, this process made me aware that this information just brings one layer of understanding, that is about assemblage. The origins of the materials and its processing remain still unknown. How to understand a full chain of production if the information is hidden? Does it matter when we have now tools and spaces that enable our emancipation of making? Rapid prototyping machines were tools created by the industry to facilitate one step of the production, but now they are accessible in price and publicly to people in makerspaces and fab labs. However, this network of making around the world is very much standardized, with the same offer of machines and use of materials. How can these spaces thrive as micro manufacturing platforms in the city to connect people to their places and empower themselves and their local narratives?
I started to investigate these questions by a first-person perspective point of view. I wanted to know the possible ways of connecting to a place through making. I decided to explore these possibilities on my own first, to then draw conclusions to formulate a proposal that could be embraced broadly by society. Since I moved to Barcelona for the master, this would also be a way to get to know this territory, its dynamics, people, culture and traditions. Is it possible that I would connect locally through making?
<br>
<br>
{% figure caption: "*All the places related to the project I visited in Barcelona*" %}
![]({{site.baseurl}}/p_t03_map.jpg)
{% endfigure %}
<br>
<br>
**Area of Interest**
<br>
<br>
The area of interest lies in the field of making and creating bonds between people and nature in this process. As a way to explore possibilities of establishing these connections between people, place and materiality, I defined to work on a research of local connection through making in the context of micro manufacturing in the city.
<br>
<br>
The intent is to make visible that design is not only an image or final product, but it is a process, it has expertise, cultural history embedded and it is materiality. In our current model of consumption, we are missing the connection with the making, since we are distancing ourselves from the act of buying in the globalized world. Moreover, production is being done in faraway lands, detached from our current much urbanized reality. So by tackling these issues, it is possible to explore how we can collectively understand design and its role in our urban lives. I believe that one path to achieve this comprehension is by local connection through making. With people through identities, shared knowledge, patterns, shapes. With nature through materials, circularity and technology.
<br>
<br>
A first approach was to look into crafts, since it is a practice inherently local - with materials and techniques. As a first-person perspective analysis, I went for classes of traditional pottery making. I learned some techniques and experienced the material with my hands. This was a design action to understand pure crafts. Then, I wanted to understand how makerspaces could have a role in the human making process. How could the Commons knowledge of making - people as humans archives, transmitting techniques and processes like craftsmanship - interact with new emerging technologies for making in the urban context? I decided to look in how 3D extruding clay would be. At the moment, there are technologies that involve the extrusion of clay with robotic arms or specific 3D printers. At Fab Lab Barcelona I had at my availability a small robotic arm. I decided to go through this process of making as a way to understand what I would learn or miss in the process. But before, I questioned myself about why would I need a machine to extrude clay. Therefore, I first tried to extrude clay by hand. I had a frustrating attempt with a big syringe, later decided to look for cooking tools as the ones for icing/ cake decoration. Also it was a hard process. In the end, I used a plastic bag with a little cut on the edge. With this tool it was possible to extrude clay by hand but I learned another issue: it is really hard to maintain a constancy of extruding material by hand. From that, I learned what would be the role of the machine in this process: to extrude material in an evenly way.  The process of extruding clay in the robotic arm was possible with the help of Eduardo Chamorro, a tutor from the Fab Lab and Barbara Drozdek, a colleague from the master. From going through the process, I learned that setting the first parameters with the machine and making the file ready for execution take a lot of time but extruding is a really fast process - around five minutes for a cup.
<br>
<br>
After presenting these design actions around the exploration of crafts and the role makerspaces could have to transmit this knowledge, I had a period of reflection in between the second and third terms. In looking for the direction to take with the project, I realized that I did not have to choose between one path or another (pure crafts x new technologies of making) but I could embrace the undefined-hybrid space that is arising from the possibilities of making in the urban context. Therefore, the intent will be to redefine production, not anymore as a production line in an industrial context, but as a lively practice of connection and circularity, inside the urban fabric. To start on this exploration, I first investigated the possibilities of using new technologies for making. With this experience, I could draw conclusions and elaborate paths related to the perpetuation of the Commons knowledge through new tools of making, considering local design expressions.
<br>
<br>
**Description of the intervention**
<br>
<br>
As mentioned in the previous topic, the intervention will be based on exploring the possibilities around the evolution of the Commons knowledge, aligned to material culture evolution. The decision was to look into a specific object that could communicate different cultural expressions. With this consideration, the proposition was to create a set of products that represent the concept of making as a lively practice of local connection and circularity in the context of urban areas. Production (or large scale production) is not the end goal, it is the result of making for the sake of connection with land and others; out of necessity and cultural expression, instead of a response to alienated standard overconsumption acts. The intervention is a critical overview over our production-consumption systems and how disconnected from the processes and decisions our society is. By experiencing as first-person perspective, the possibilities of producing in the urban context were analysed. This considered a sequence of processes, looking for the source of materials, the incorporation of micro manufacturing and human inputs. The humans bring their cultural factor to the project, interacting with a parametrized design that can take into account their local expressions - patterns and symbols from their realities. Considering that we are living in the age of mass displacement, a next iteration for the project is to incorporate, besides the human knowledge from locals, the Commons knowledge from the ones displaced, whom are in much need of connection to the new land they are now inhabiting. This chain of creation is based on a nature+machine+human+humans interaction.
<br>
<br>
The set of products defined to be explored were culture-defined plates. Food is the most clear cultural trace and one of the best acts of connection between people. Each culture has its own way of displaying food - based on the availability of ingredients during seasons, of scarcity or abundance and from their societal definitions and political affairs. Thus, the intention was to shed light on the most candid support (plates) for the most direct cultural expression (food) with a design intervention.
<br>
<br>
**State of the art**
<br>
<br>
{% figure caption: "*Atelier NL - Earth Alchemy Factory*" %}
![]({{site.baseurl}}/p_t03_ateliernl.jpg)
{% endfigure %}
Atelier NL - Earth Alchemy Factory: The Eindhoven-based studio is researching natural raw materials from different regions in the Netherlands and also from around the world. With the project “Earth Alchemy Factory” they invite people to learn about these materials through making. They developed workshops, lectures, archives and material libraries to bring design, education and production into one system. In their own words, “we believe that understanding natural material transformation encourages more sustainable cultural evolution”.
<br>
[http://www.ateliernl.com/](http://www.ateliernl.com/)
<br>
<br>
{% figure caption: "*Superlocal*" %}
![]({{site.baseurl}}/p_t03_superlocal.jpg)
{% endfigure %}
Superlocal is a project that intends to create a global network for local manufacturing. Through design actions of local production tours and lectures, Andrea de Chirico, its creator, wants to foster a future vision on crafts and production. He started from the Fab Lab network, adding a new layer, which looks into traditional manufacturing techniques. The intention is to generate global impact by creating a network of people that connects resources and labour locally, helping their local economies, craft traditions and encouraging new manufacturing mentalities.
<br>
[https://www.super-local.org/](https://www.super-local.org/)
<br>
<br>
{% figure caption: "*Super-local*" %}
![]({{site.baseurl}}/p_t03_super-local.jpg)
{% endfigure %}
The Dutch studio Super-local, currently based in Africa, develops their work around design products, services and systems to improve the quality of the lives of the poor. They immerse themselves in a local unprivileged reality to help people identify their skills and knowledge, developing a hands-on approach and in collaboration with local communities.
<br>
[http://www.super-local.com/](http://www.super-local.com/)
<br>
<br>
{% figure caption: "*A factory as it might be - project*" %}
![]({{site.baseurl}}/p_t03_assemble.jpg)
{% endfigure %}
“A factory as it might be” was an architectural installation at A/D/O by Assemble, Granby Workshop, Will Shannon & collaborators, which consisted in a set up of a model factory, equipped with clay and an industrial extruder. The idea was to explore the concept of an itinerant production, by transferring the experimental production approach of Granby Workshop, located in Liverpool to New York, where the installation occurred.
<br>
[https://assemblestudio.co.uk/projects/a-factory-as-it-might-be](https://assemblestudio.co.uk/projects/a-factory-as-it-might-be)
<br>
<br>

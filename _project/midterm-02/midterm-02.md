---
title: 03 • Reflections Midterm 2
period: 05 March 2019
date: 2019-03-05 12:00:00
term: 2
published: true
---


 <br>
 <br>
 Midterm presentations (05.03.2019) took place to orient the development of the research into our areas of interest. We had our tutors and guests to comment on our projects. I had the chance to hear feedback from Mara Balestrini, Ingi Freyr and Tomas Diez.
<br>
<br>
As presented in the previous term, during Design Dialogues, my area of interest lays in the field of making and how this process can create bonds between people and the place. I followed on this field and presented the advances on **local connection through making - material reconnection in the context of the redistributed manufacturing world**.
<br>
<br>
As a way to evolve with the research, I decided to look into crafts, as it is a set of techniques inherent from a specific place. As a design action, I decided to go to a ceramic class, a type of crafts I was not familiar previously. In the past, I have already experimented with painting, wood work, weaving, jewelry making and fusing glass. This contact with the ceramics making brought interesting insights that are mentioned in the presentation. The tutor shared details about clay provenance and specific regions devoted to ceramics education in the region, as in La Bisbal. Also, we could talk about how this craftsmanship has faced an increase of interest in the last couple of years. The experiential impressions about working with clay are also detailed in the presentation.
<br>
<br>
Also as part of the research, I went to design and crafts fairs, to understand what is being made in Barcelona, what materials are most used, if traditions are being kept or if the creators are innovating and incorporating new techniques or technologies in their works. It was very valuable to have talks with these people and get to know the current scene of creators in the city. I collected some cards of these craftsmen/women and displayed at the presentation, along with a ceramic piece and also the work done during the Material Driven Design course, which was a much valuable class to strengthen the dialogue with materials. I showed the spoon we carved from wood on the beginning of the course and also the material from avocado seed I developed throughout it.
<br>
<br>
{% figure caption: "*objects displayed during the presentation*" %}
 ![]({{site.baseurl}}/p_mt02_01.jpg)
 {% endfigure %}
<br>
<br>
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQSDXpXWcPVIscifiwoQ0c4H2jT38CPakWHbdoEwOUpgPWplBxftJwnal-AfobutGnwvt7FvlHe_xGs/embed?start=false&loop=false&delayms=3000" frameborder="0" width="680" height="424" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
<br>
<br>
As feedback from my presentation, I heard many interesting projects to look for and also about how I should conduct my work next. Below, I will describe the comments from each tutor/guest.
<br>
<br>
• Mara Balestrini
<br>
<br>
On her view, I presented a very interesting topic and with confidence, but I had too many questions. She believes that I know the right question to be asked and that I have to narrow down to this one topic from the material world that I want to talk about.
<br>
<br>
• Tomas Diez
<br>
<br>
In his opinion, my work was presented in a clear and organized way, impeccable, but I should start making decisions on where I want to take the project. He sees that I should make uncomfortable statements and be less polite, I should play with making people uncomfortable to form a debate. Also, to try to not speak about craftsmanship in a way that will look to it with romanticized lenses, looking to a past and trying to bring it back. He suggested to have a conversation with Jonathan Minchin about the project [Open Lab](http://openlab.org/).
<br>
<br>
• Ingi Freyr
<br>
<br>
He also suggested to define better my direction, my end goal. About craftsmanship, he sees it as a very broad field to work on, I should be more focused on a specific thing. He suggested to take a look at the project [Cross Cultural Chairs](https://crossculturalchairs.com/) as it talks about different cultural expressions around the world over one single object - the chair.
<br>
<br>
**Conclusions**
<br>
<br>
It was good to hear that my presentation was coherent and well structured, although as I  presented many questionings, it was hard to identify a core question to be deepen in the presentation.The fact that it was hard to decide which path to go on, shows that a work around this must be developed till the end of the term.

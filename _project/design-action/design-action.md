---
title: 07 • Intervention’s Design Action and Results
period: 14 June 2019
date: 2019-06-14 03:00:00
term: 3
published: true
---


<br>
<br>
To present the intervention intended, its action and results, I will deploy its process in three topics: Understanding, Making and Sharing.
<br>
<br>
*Understanding*
<br>
<br>
The intention of this project is to foster local connection through the act of making, in the context of urban areas. As I began to define the project, I started to do some design actions that would lead me to a greater comprehension of the making in the city. My first approach was to look into crafts, as it is a practice inherently local, in both material and cultural realms. Craftsmanship also carries an evolution to the Commons knowledge, being developed in a specific context but also being spread around the world for reinterpretation from different cultures.
<br>
<br>
In the realm of crafts, I decided to explore the world of ceramics, since it was a practice I was not previously familiar with and also for the close relationship of it with the Iberian territory - from extraction and production of many products out of clay. My first design action was to attend to ceramic classes to learn basic techniques - I learned the principles of coil construction, slab construction and wheel throwing. I also had conversations with designers and ceramists to understand about the context of their work. Ceramists and teachers Marc Vidal and Julen Ussia shared their trajectory and did demonstrations of wheel throwing. Laia Pascual, ceramist and teacher, showed the ceramics workshop of the school ‘Escola d’ART La Industrial’ and shared her opinions over materials’ provenance and different techniques and finishes’ implications. Claudia Capaccio, with whom I had some of the basic classes, shared her trajectory and impressions over the presence of ceramics in the Catalan territory, mentioning her formation at La Bisbal. Capaccio also shared her view over the ongoing popularization of artisanal ceramics. In the realm of new technologies, I came to know spaces and people printing with clay. In the city of Vilafranca del Penedès, ‘Ceràmica Baltà’ is a traditional ceramic workshop that decided to also explore 3D printing techniques. Bernat Cuni, a product designer, explores digital craftsmanship under his studio Cunicode and was generous to share his knowledge and ideas over this new field. Other studios working with micro manufacturing of pottery are Noumena, Coudre Studio and Mnok Studio. The process of exploring the Commons knowledge of ceramics in Barcelona was also a tool for getting to know the city better and its dynamics around the creative industries, the sources and partners for the production in this field. From this comprehension of traditional and new ceramics expressions in Barcelona, I was able to fundament better the intended intervention.
<br>
<br>
{% figure caption: "*Marc Vidal studio visit*" %}
![]({{site.baseurl}}/p_t03_studio-visit.jpg)
{% endfigure %}
{% figure caption: "*Learning pottery techniques*" %}
![]({{site.baseurl}}/p_t03_pottery.jpg)
{% endfigure %}
<br>
<br>
After experiencing the traditional knowledge of pottery making, I decided to see how this craft could be done in the context of a makerspace, as part of the exploration of possible futures to safeguard cultural heritage while decolonizing design by the empowerment of local narratives. I wanted to experience how extruding clay with a machine would be and understand what feelings or experiences could be missed or learned in this process. But before, I wanted to understand why I would need a machine to extrude clay, and so, I first tried to extrude clay myself, by hand. Instantly I realized that is really hard to control the flow of clay, to keep a consistency in the extrusion of the material. With the machine, you have a compressor or a motor over a syringe that is pushing the material out and making it go out evenly. Then I moved to experience the process with a machine. The process of using it, in the case a small robot, to extrude clay is very time demanding on the first steps. To prepare the file, to define the settings and fill the extruder with clay was a process that took more than an hour to happen. But when it comes to extruding, it is really effective, according to the file to be printed evidently, but a simple cup took around five minutes to be done. With the machine already set, many files can later be printed in a short period of time. The potential of micro manufacturing in cities can be an amazing revolution for shifting from a model based on industry production, centralized, with a lot of transportation logistics to be done. Also, enables people’s empowerment in reflecting over the underlying politics of objects - Why this shape? Why this size? Why these colors or decorations? What all these characteristics tell me?
<br>
<br>
{% figure caption: "*Experiment: extrusion by hand*" %}
![]({{site.baseurl}}/p_t03_hand-experiment.jpg)
{% endfigure %}
{% figure caption: "*Experiment: first extrusion with a robotic arm*" %}
![]({{site.baseurl}}/p_t03_robot-experiment.jpg)
{% endfigure %}
<br>
<br>
*Making*
<br>
<br>
After gathering the understanding described, the proposition was to make something physical of the learned process, still in a first-person perspective to gain knowledge and be able to formulate the intervention. As previously described, the decision was to make culture-defined plates. From a parametric design file, people would bring their cultural bias and interact with it, changing parameters related to its size, shape and in accordance of the clay used. The digital object can be materialized by extrusion of clay with the help of a robotic arm or a 3D printer adapted to extrusion. Some post-production is needed in order to become a functional plate. Moreover, other specific expressions can be added, as drawings and paintings.  The process started at looking in the input for the parametric design. As the exploration was about looking into Commons knowledge of craftsmanship, I searched for specific regions of pottery making to source data as an input for making some reference pieces. The regions selected will be further explained, but were: Serra da Capivara in Brazil, Mashiko in Japan and Safi in Morocco. The parameters of plates (dimensions and shapes) from tableware of these pottery regions were introduced in the parametric design, done in the software Rhinoceros with the plugin Grasshopper. In the file, it was considered the diameter, the height and the inclination of the internal part.  Since ceramic 3D printing has limitations of production, as it is unable to do angles lower to 45º, the shape of these plates were adapted to these constraints. This is a learning from the process, to understand that novel technologies can present limitations in favour of traditional making techniques.
<br>
<br>
<br>
![]({{site.baseurl}}/p_t03_parametric1.jpg)
{% figure caption: "*Studies for parametric design*" %}
![]({{site.baseurl}}/p_t03_parametric2.jpg)
{% endfigure %}
<br>
<br>
For the materials input, it was considered the percentage of shrinkage. Since it would be hard - or even impossible - to find clay from these places here, I am using clay available in Barcelona. It is produced in Esparreguera (45 km away from Barcelona) and I chose to work with grogged red earthenware. There is this element of displacement in which plates inspired by other cultures were done with a clay from the Iberian territory. However, I do not see this as a big contradiction, since it is an exercise, a process for prototyping the idea and making it visible for the audience.
<br>
<br>
{% figure caption: "*Grogged red earthenware*" %}
![]({{site.baseurl}}/p_t03_clay.jpg)
{% endfigure %}
<br>
<br>
As mentioned before, at Fab Lab Barcelona there was a small robotic arm available for ceramic 3D printing. In the process with the robot, it is needed to prepare the toolpath - a group of lines - that will materialize the object. The way these lines are set will define the end result, for shape definition. This quality depends of the layer height (another parameter in the design file) as smaller heights give better definition for the object’s format. In the experimentations done, the layer height was of 2.5mm, not being that much small, so it is possible to visualize the layers of deposition of the clay. Post-production could be done to hide the layers, but as a way to evidence the process, I decided to keep then in the end result. On the first attempt to print a real-sized plate, it was possible to realize that the cartridge that contains the clay to be extruded would not be enough. The idea of scaling the plates to make it possible to print in one go was not appealing, since the idea was to transpose the reality of the inspirational objects. Therefore, the decision was to prototype portion of the plates, so it would be possible to analyse the format of the different culture-defined plates. There were four sessions of prototyping, all with the help of Fab Lab tutor Eduardo Chamorro. In each of the sessions, one aspect of previous learnings were improved.
<br>
<br>
{% figure caption: "*Process*" %}
![]({{site.baseurl}}/p_t03_process.jpg)
{% endfigure %}
{% figure caption: "*Some results*" %}
![]({{site.baseurl}}/p_t03_results.jpg)
{% endfigure %}
<br>
<br>
About the selection of places to take inspiration for the experimentation with new technologies, the research started in looking for information about typologies of tableware. Soon it was visible that it is hard to find proper research on the field, since the information available is either from archaeological context or specific to pottery studios or movements. It is hard to find scientific information about how culture and politics shaped tableware in different places. Yet, some non-scientific information was found and as a strategy, I decided to look into specific pottery productive regions. It was interesting to approach in this way since many of these areas are based on this one activity, pottery making. In most cases, this specialization was defined by the availability of clay locally. With the development of techniques through time, the highly specialized knowledge gave them recognition in the making. This also speaks about territory and how the act of making has been organized around the specialization of areas or cities according to available resources. With the possibilities of doing anything, anywhere, these days, how material culture evolution will be deployed in the futures?
<br>
<br>
The regions selected for inspiration were mentioned before, now I will detail information about each of them.
<br>
<br>
• Serra da Capivara, Brazil: It is a national park in the Northeastern region of Brazil. The area has the largest and the oldest concentration of prehistoric sites in the Americas and the park was created to protect the prehistoric artifacts and paintings it contains. Some of the discoveries done there lead to a reevaluation of fundamental traditional theories related to the origins of human settlement in the Americas. The area became a UNESCO World Heritage Site in 1991. The head archaeologist is Niède Guidon, who does enormous efforts to keep the park open. A lack of national public budget to maintain it makes things difficult and most of the support comes from foreign investments. As a way to create new dynamics in the area, a museum and an artisanal pottery group was formed. The pottery is done at the “Barreirinho” village, a rural area located in the municipality of “Coronel José Dias”, near the conservation area of the park. Through pottery, the importance of the national park is communicated to a broader audience, as it is being sold in other regions of the country too. From the catalogue of the pottery group, I took out measurements of a plate to inspire the making of one with ceramic 3D printing. The diameter is 260mm and the height is 20mm.
<br>
<br>
{% figure caption: "*Plate from Serra da Capivara pottery group*" %}
![]({{site.baseurl}}/pt_t03_br.jpg)
{% endfigure %}
<br>
<br>
• Mashiko, Japan: The making of pottery in this city is said to have started in the Edo era, with Keisaburo Otsuka, who was trained in Kasama and then moved to Mashiko and built a kiln. Since the area was known for an excellent kaolin and also is close to Tokyo, the production there developed specially around pots, urns, water jars and other items of daily use. A bigger recognition came when Shoji Hamada decided to settle there in 1924, after living a period in the UK and briefly in Okinawa. Hamada pushed the folks art movement in Japan (Mingei), creating a special identity for the pottery done there, the Mashiko ware, which was defined having “functional beauty”. He collected folk art pieces from around the world as inspiration, but developed a characteristic work with the local clay and glaze. Once he said: "In Kyoto I found the way, in England I started, in Okinawa I learned, in Mashiko I grew mature”. It is interesting about this quote to see how his trajectory happened in different territories and how from each place he learned something to transmit in another land and transform according to his identity. Material culture evolution does not happen only in one place; since humans started to exchange with different cultures through travels, knowledge has been shared and transformed according to local contexts. Since Japanese food culture revolves around eating in bowls much more than in plates, I decided to take as inspiration the udon bowl of the Mashiko ware. The diameter is 160mm and the height is 110mm. When analysing the Japanese tableware, I was surprised to discover that in some situations, the size of the bowl might be defined by the role in the family, which is also, in a way, is attached to gender. In some cases, the father might have a bigger rice bowl (chawan) than the mother. The rice bowl is also the most personalized item of the tableware, being quite often exclusive to that person in the family.
<br>
<br>
{% figure caption: "*Udon bowl from Mashiko ware*" %}
![]({{site.baseurl}}/pt_t03_jp.jpg)
{% endfigure %}
<br>
<br>
• Safi, Morocco: The majority of this country’s pottery is done in three cities, accounting for 80% of its production. They are Fez, Meknes and Safi. The craftsmanship there is centuries old and passed on through families. The main technique explored is with the pottery wheel and tableware is very much decorated, with colorful etchings and glazes. Since is highly influenced by Islamic or Berber art, commonly is adorned with complex geometric or arabesque drawings or floral motifs. Safi is considered the capital of Moroccan pottery, hosting the National Ceramic Museum. The pottery from Safi is often made of red clay and glazed in green, turquoise and black, and also is known for having metal inlays. Food in the Moroccan culture is mostly shared, so a plate taken as inspiration was relatively big, with 270mm of diameter and 50mm of height. Very unique to their culture is the tagine, a conical-shaped cooking pot for making the dish with same name. The utensil keeps the condensation of the heated food, usually being a slow-simmering stews with meats, vegetables and sauces. In most countries of Africa, the process of pottery making comprehends the whole process - from extracting the clay, making the pottery and firing it. In Safi, the clay is extracted locally and the firing can happen in gas powered kilns or traditional wood fired kilns.
<br>
<br>
{% figure caption: "*Plate from Safi's pottery*" %}
![]({{site.baseurl}}/p_t03_ma.png)
{% endfigure %}
<br>
<br>
After the making part of the project, the idea for next iteration, which will be described in the topic below, was about sharing this project among the maker’s network and encourage people to do the plates related to their context. It would be a very interesting output if makers from different places start to look at the specific techniques of pottery executed in their contexts and start to collaborate with these craftspeople in order to foment both worlds, the ancient local knowledge and the open/global connected network of making. Since at the stage of prototyping, I had difficulties in producing a real-sized plate, the idea of sharing the project with other maker’s hasn’t advanced at the moment. The initial idea for the project was that it would be accessible to be made, and by realizing that the task of making plates requires a more robust machine and employs a lot of time for printing and also a lot of material, I believe that is a valid prototyping as exercise around material culture evolution, but yet, with the technology available at the moment, it is not a reality able to be made.
<br>
<br>
*Sharing*
<br>
<br>
As introduced in the topic before, the idea was that the project would be shared online globally, in order to be appropriated by makers who could put it on action in their local context. Since the focus is on pottery and clay extrusion, the first step would be to contact people from the 3D printed clay community, either directly or looking into platforms where they share their knowledge, for example, Wikifactory. This intent is to create a repository of this local knowledge and different expressions according to the specificities of each place. This could be shared in a platform where people can learn stories from different localities and see similarities with their own culture. Also, it could be the starting point for makers to establish a new kind of production, considering micro manufacturing in the urban context as a lively practice of connection and circularity with their lands. Connecting people, materials and place are the keywords of this project. This part of the project can be a next iteration around other artifacts, the kind of things that are possible to be done with the technology available. Also, to push the limits for technology breakthrough would be interesting. Even though the initial idea of making plates was not completely successful at the moment, I see that sharing this experience is valuable, in order to promote reflection around cultural expressions and how we need to foster this, in order to better connect with our lands and to fight the homogenization of expressions carried in the wave of global capitalism. By sharing this learning, trying to engage makers around the world to look for their local available resources and possibly doing workshops to explore the hybrid space in between the Commons knowledge of craftsmanship and the new emerging technologies for making in the urban context, I believe that the message of this project - the intervention -  is going to be well communicated.
<br>
<br>
*Results*
<br>
<br>
As already explained previously, the results for the making process were not much what was expected, reflecting in a deceleration for the deployment of the sharing part. The understanding process was very enriching, expanding perspectives and providing a connection to the city’s dynamics. It was a way for me, a temporary immigrant in Barcelona, to connect better with this territory and to get to know its people, the ones who are from here and also others in the condition of migration.
<br>
<br>
Exploring different techniques with clay can bring up surprises, and to go through the phases of materializing, post-production, firing and glazing are processes that would confirm if the design could be manipulated in different shapes and sizes. The concept is possible to be executed, yet, at the stage of the available technologies, it became clear that would not be an efficient or sustainable process. In that sense, the Commons knowledge has still the best answer for producing plates, and it is important to acknowledge this fact and cherish the existence of such traditional craftspeople techniques.
<br>
<br>
![]({{site.baseurl}}/p_t03_exp1.jpg)
<br>
<br>
![]({{site.baseurl}}/p_t03_exp2.jpg)
{% figure caption: "*Experiments with the robotic arm*" %}
![]({{site.baseurl}}/p_t03_exp3.jpg)
{% endfigure %}
<br>
<br>

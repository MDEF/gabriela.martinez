---
title: 08 • Sustainability Model
period: 14 June 2019
date: 2019-06-14 04:00:00
term: 3
published: true
---


<br>
<br>
From the seventeen UN Sustainable Development goals for 2030, numbers eleven and twelve are respectively “Sustainable Cities and Communities” and “Responsible Consumption and Production”.
Among the many targets, it is possible to highlight two that connect directly to this project:  “Strengthen efforts to protect and safeguard the world’s cultural and natural heritage” and “By 2030, ensure that people everywhere have the relevant information and awareness for sustainable development and lifestyles in harmony with nature”. The proposition of local connection through making happens in order to deepen the bonds of people with their places, the local materials, expressions and communities, creating a reflection about current lifestyles and the needed reframing of them. Many traditional crafts have an awareness about the usage of resources and creating for durability. Spaces of making have tools and machines that enable creations and recreations of techniques and cultural knowledge. By connecting them and presenting to the community possibilities to learn how to become agents of production is a step towards the goal of reaching self-sufficient cities.
<br>
<br>
As a sustainability model for this project, the first step would be the practice of teaching - workshops, lectures or consulting are ways of transferring knowledge that will help people change behaviours and act in their personal lifestyles or in their professional practices towards a shift from current models. Teaching can also be a valuable tool to give people skills in the context of job loss or migration, empowering them to develop their own creativity and cultural expression tied to the urban fabric. A next step would be, considering the existence of a making space, the structuring of a real production environment, creating a fair and balanced chain of manufacture of objects for everyday life. This could be set with craftspeople that are willing to explore new techniques, experienced makers that want to attune better to a cultural contextualized creation or even newcomers to the field, that will experience a training before exploring their own skills. The revenue from this model could come from the shared usage of the space through a community fee or even the structuring of store/gallery that would sell the creations in support of the community.
<br>
<br>
These concepts are not disruptive in any sense, being explored by regular studios, makerspaces or Fab Labs. It is a combination of possibilities to promote the values that are believed by the proposal, considering a conscious usage of resources, ethical and healthy labour and thoughtful circularity for the whole process.

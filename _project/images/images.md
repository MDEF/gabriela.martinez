---
title: 11 • Five Key Images
period: 14 June 2019
date: 2019-06-14 07:00:00
term: 3
published: true
---


<br>
<br>
{% figure caption: "*Understanding the Commons knowledge - the craftsmanship of pottery making*" %}
![]({{site.baseurl}}/p_t03_01.jpg)
{% endfigure %}

{% figure caption: "*Understanding techniques - Why to use machines? Test of extrusion by hand*" %}
![]({{site.baseurl}}/p_t03_02.jpg)
{% endfigure %}

{% figure caption: "*Understanding new technologies - ceramic 3D printing by extrusion*" %}
![]({{site.baseurl}}/p_t03_03.jpg)
{% endfigure %}

{% figure caption: "*Making with digital fabrication - culture-defined plates*" %}
![]({{site.baseurl}}/p_t03_04.jpg)
{% endfigure %}

{% figure caption: "*Process of making - ceramic 3D printing with a small robotic arm*" %}
![]({{site.baseurl}}/p_t03_05.jpg)
{% endfigure %}

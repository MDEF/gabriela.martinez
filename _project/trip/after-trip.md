---
title: 02 • Reflections After Research Trip
period: 07-19 January 2019
date: 2019-01-21 12:00:00
term: 2
published: true
---


<br>
<br>
During the first two weeks of the second term, I developed a project in Barcelona with other colleagues and went for a research trip to Copenhagen with some of them.
<br>
<br>
The project developed in Barcelona was inspired in the Make Works project, which as the objective to map manufacturers and makers in a city. The students taking part were Aleksandra Łukaszewska, Alexandre Acsensi, Emily Whyman, Gabriela Martinez, Jessica Guy and Veronica Tran. We had one week to deploy this project in the neighborhood of Poblenou. As a form of strategy, we decided to visit makers we already knew and let them guide us to new makers. This process was documented in video and audio interviews, later to share it on a website and instagram account. The idea of a social network was to engage people in taking part of the project here or in their own cities. We called the project *Make Here Barcelona* and the idea is that any new city could make their *Make Here + City name*. As a way to make the process understandable to this new network, we created a toolkit to inform people how to start the project in their own cities. The whole idea of *Make Here* is to share information about local making and create a community. We want to connect people who make to people wanting to make.
<br>
<br>
More information about the project is available in my post about the [first week of Design Studio](https://mdef.gitlab.io/gabriela.martinez/term-2/01_ds/). This project helped me to understand better the relationships that exists in the Poblenou district and to visualize how the act of making and the act of helping others to make are happening here. Since I want to develop my final project in a field where the act of making deepens the connection between people and the place where they live, it was very much relevant to hear the experiences of the makers of Poblenou. They shared their trajectory, their processes and tools of choice, where they usually source materials and how they connect with others nearby.
<br>
<br>
After developing the project, some of us went to a research trip to Copenhagen. Alexandre Acsensi, Emily Whyman, Veronica Tran and me organized visits to some places of interest in the city. Besides a regular sightseeing of places and visiting museums, we contacted some people/spaces from the network of IAAC to visit there. The first space was [Underbroen](http://underbroen.com/), a makerspace under a bridge in the city center. They also have other spaces around the town, which people can go and make projects. It was really interesting to visit their facilities and get to know their important role in the maker community of Copenhagen.
<br>
<br>
 {% figure caption: "*Underbroen*" %}
 ![]({{site.baseurl}}/p_trip_01.jpg)
 {% endfigure %}
<br>
<br>
Later, we visited [SPACE10](https://space10.io/) a design and research lab. SPACE10 tackles issues we face or will be facing shortly in matters of urbanization, mobility and societies’ lifestyles. They investigate UN macro trends to come up with possible scenarios to solve our problems, most of the times, in partnership with avant garde design studios. It was really interesting to get to know their approach over topics of research and the processes they implement to develop exciting and engaging narratives and projects.
<br>
<br>
 {% figure caption: "*SPACE10 maker room*" %}
 ![]({{site.baseurl}}/p_trip_02.jpg)
 {% endfigure %}
<br>
<br>
We also visited [KEA Material Lab](http://materialdesignlab.dk/), coordinated by Mette Bak-Andersen, our tutor for the Material Driven Design class. We could see a vast collection of materials from the Material Connexion network and also a very special library of materials sourced from nature, which was really informative and inspiring. Moreover, Bak-Andersen showed us the facilities of the Material Lab and commented on some projects developed there. It was really inspiring to hear from her that the exploration on materials enable us to challenge the block of material innovation currently done by technology. Tools are limiting the diversity of materials and local expressions done with them.
<br>
<br>
 {% figure caption: "*KEA Material Lab*" %}
 ![]({{site.baseurl}}/p_trip_03.jpg)
 {% endfigure %}
<br>
<br>
 {% figure caption: "*KEA Material Library*" %}
 ![]({{site.baseurl}}/p_trip_04.jpg)
 {% endfigure %}
<br>
<br>
To conclude the trip, we visit the project [Next Food](https://nextfood.co/), which is also being developed in Barcelona. It is an indoor farm system through aeroponics, cultivating vegetables and fruits in a controlled environment, giving plants the needed amount of water and nutrients. The project is open source and was the winner of the Distributed Design Award in 2018.
<br>
<br>
The visits during the Copenhagen trip made me visualize the importance of collaborations and information sharing in a network and how global connections with bigger initiatives can have a positive impact locally. To focus in your community is really important to develop relationships that will last and contribute to a further development of actual projects and possible future ones.

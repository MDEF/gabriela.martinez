---
title: 04 • Reflections Term 2
period: 09 April 2019
date: 2019-04-09 12:00:00
term: 2
published: true
---


<br>
<br>
The final presentations for this term of Design Studio (09.04.2019) promoted a space of discussion of our master’s projects with many guests. Again, we exhibited our projects and guests or tutors would come to talk with us.
<br>
<br>
For this day, I presented some additional design actions done since the [midterm](https://mdef.gitlab.io/gabriela.martinez/project/midterm-02/) review. Since I am talking about **local connection through making** in the context of urban areas, I decided to do a design action that had relation to spaces of making in cities. Fab Labs are an incredible network around the world to provide this possibility. As I started to look into crafts, as a way to look into the context/culture of a place, I decided to see how this craft could be done in the context of a makerspace. After doing a ceramics class, I decided then to see how extruding clay with a machine would be. I wanted to understand what feelings or experiences could be missed or learned in this process. But before, I wanted to understand why I would need a machine to extrude clay, and so, I first tried to extrude clay myself, by hand. Instantly I realized that is really hard to control the flow of clay, to keep a consistency in the extrusion of the material. With the machine, you have a compressor that is pushing the material out and making it go out evenly. The process of using a machine, in the case a small robot, to extrude clay is very time demanding on the first steps. To prepare the file, to define the machine settings and fill the extruder with clay was a process that took more than an hour to happen. But when it comes to extruding, it is really effective, according to the file to be printed obviously, but the simple cup I wanted to print took around 5 minutes to be done. Since the machine was already set, we printed out a second cup, and as it can be done, the file could be easily printed many times in a short period of time. These machines enable micro manufacturing in the cities and this can be an amazing revolution when we are talking about shifting from a model based on industry production, centralized, with a lot of transportation logistics to be done. But how this relates to crafts? And culture? How this can enable a local connection with the place it is being produced? These were some of the questions that came to me after presenting to guests and tutors, who also gave me some interesting questions to analyze from now on, till the end of the project.
<br>
<br>
{% figure caption: "*Project exhibition*" %}
![]({{site.baseurl}}/p_t02_01.jpg)
{% endfigure %}
<br>
<br>
<iframe width="672" height="378" src="https://www.youtube.com/embed/cLuZDkM6rCw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<br>
<br>
{% figure caption: "*Mapping of weak signals related to the project*" %}
![]({{site.baseurl}}/p_t02_02.gif)
{% endfigure %}
<br>
<br>
{% figure caption: "*Speculative scenario for 2050*" %}
![]({{site.baseurl}}/p_t02_03.gif)
{% endfigure %}
<br>
<br>
The feedback I received from tutors and guests was positive and interesting. Many of them highlighted that my narrative was strong and coherent. Anyhow, some of them asked for a more clear definition for the intervention I will be deploying on the next trimester. Below, I indicate the most interesting insights and feedbacks I received from them as a way to improve my project.
<br>
<br>
**Laura Cléries**
<br>
Keep objective on my purpose. I should explore more the concept of creativity - local manipulation as expression of local creativity.
<br>
<br>
**Lucas Peña**
<br>
As I am talking about objects as a form of heritage objects and I am considering to work with communities, I should have the community tell me why certain object or technique is important.
<br>
<br>
**Chiara Dall’Olio**
<br>
She mentioned that it is interesting to see how local narratives are missing in the context of fab labs. Fab labs are considered as a place for anyone to make almost anything and in a personalised way, out of the industry or standardization, but in the end, the results are very similar, this being more visible in the materials list. Also, she asked me to reflect on how my action will be a proof of my research.
<br>
<br>
**Jonathan Minchin**
<br>
I should exchange information with other IAAC students working with clay. I should explore the potentialities of the machine in a more unconventional way.
<br>
<br>
**Domestic Data Streamers (Marta Handenawer and Pol Trias)**
<br>
I should reflect on how I am defining the identity of these objects to be linked to a particular place. I should search for creative processes that bring more interaction of the body with the machine. How the human factor could have more participation in the output created by a machine? So, look to give inputs to the machine that are more creative and linked to body movement. One thing about my project that is really interesting and that I should not loose in the future is the exploration with processes but having a reflection in human feeling, this is important because I am talking about culture and crafts.
<br>
<br>
**Oscar Tomico**
<br>
If I am going to do workshops, I should do many to iterate and improve in the next ones. Since they will be happening often, ideally this should become part of a fab lab.
<br>
<br>
**Tomas Diez**
<br>
Maybe my project could benefit from different environments. Look for other makerspaces or fab labs in Barcelona. Try to understand their role and propose workshops with them.
<br>
<br>
**Mariana Quintero**
<br>
I should structure very well the possible workshops to be held - what is the goal, who is the audience, what is the expected outcome. Maybe instead of working with a community, a group, I should search for a craftsperson and try to learn from his/her knowledge and see how I can introduce this person to makerspaces.
<br>
<br>
**Liz Corbin**
<br>
I can choose to work with three different approaches: community driven, data/digital driven or have an individual approach. Also, when talking about materials, maybe it is more interesting to work with a material and to research how different cultures perceive it. Because cultures perceive and value materials differently and she thinks this is a more interesting talk to have, due to the political context of a place, the economic reality of certain communities, the technological infrastructure of different communities and their social fabric. She brought up the idea to look into waste material and how working with it can make people perceive it differently. Also, she recommended me to talk with designers-anthropologists or museums curators to understand how they apply a method of working with cultural heritage and later validate the results in their research.
<br>
<br>
**Conclusions**
<br>
<br>
I believe that I received many insightful feedbacks and that I need a bit of time to process and digest them all in a way that I can incorporate ideas to the project. The spring break will be a valuable moment to evaluate what can be added or even changed in the project so it can move forward to the intervention. What is certainly defined is that I want to develop an intervention that touches the three main topics I identified as key to this project: people, place and material. Also, the act of making is part of it as it is a process to create the social and material reconnections I believe are needed for our times. Since currently we can find spaces to make in our urban areas, with machinery that is accessible, my goal is to deploy an intervention to empower people to use these structures, in order to create things that best fit their realities and needs, in accordance to their localities. It is important to create local narratives for a more diverse and plural world, which will be truly decentralized and more democratic.

---
layout: project
title: Project
permalink: /project/
---

**Local connection through making**
<br>
<br>
This project seeks to explore the hybrid space in between the Commons knowledge of craftsmanship and the new emerging technologies for making in the urban context. It looks to safeguard cultural heritage for the futures while decolonizing design by the empowerment of local narratives.

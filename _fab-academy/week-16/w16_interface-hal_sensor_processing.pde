/**
 * Mixture by Simon Greenwold / Edited by Barbara Drozdek and Gabriela Martinez - MDEF-IAAC 2019
 * 
 * Display a shape with three different kinds of lights. 
 */
 
import processing.serial.*;

Serial myPort;  // Create object from Serial class
float val;      // Data received from the serial port
float sensorData; // Data received from the serial port with 1,2,3,4 framing numbers filtered out
float low=0;
float med=0;
float high=0;
float value=0;
float byte1 = 0;
float byte2 = 0;
float byte3 = 0;
float byte4 = 0; // change the value to float
float nsamples = 100.0; // copy from the python file

void setup() {
  size(640, 360, P3D);
  noStroke();
    println(Serial.list());
  myPort = new Serial(this, "COM4", 9600);
}

void draw() {
   while (myPort.available() > 0) {    // If data is available
    byte1 = byte2;
    byte2 = byte3;
    byte3 = byte4;
    byte4 = low;
    low = med;
    med = high;
    high = myPort.read();

    if ((byte1 == 1) & (byte2 == 2) & (byte3 == 3) & (byte4 == 4)){                // Filter out the framing numbers: 1,2,3,4
       value = (256*256*high + 256*med  + low)/nsamples;}}
  background(255);
  translate(width / 2, height / 2);
  
//  // Pink point light on the right
  pointLight(255, 100, 51, // Color
             200, -150, 0); // Position
//
//  // Purple directional light from the left
  directionalLight(69, 0, 160, // Color
                   1, 0, 0); // The x-, y-, z-axis direction

  // Cyan spotlight from the front
  spotLight(175, 255, 251, // Color
            0, 40, 200, // Position
            0, -0.5, -0.5, // Direction
            PI / 2, 2); // Angle, concentration
  
  rotateY(map(value, 0, width, 0, PI));
  rotateX(map(value, 0, height, 0, PI));
  triangle(60, 150, 116, 40, 172, 150);
}

---
title: 16 • Interface and Application Programming
period: 08-15 May 2019
date: 2019-05-15 12:00:00
term: 3
published: true
---


<br>
<br>
Assignment: write an application that interfaces with an input and/or an output 
<br>
<br>
For the *interface and application programming* week, I worked with Barbara Drozdek again since we did *inputs device* week together and for this one we are using our *hello.mag.45* board, the one that has the hal sensor to measure magnetic fields. As we did the assignment for that week, we had already been in touch with interfaces to visualize the measurement of magnetic fields. We downloaded a set of interfaces developed by [Carolina Vignoli](http://archive.fabacademy.org/fabacademy2016/fablabbcn2016/students/346/Week16.htm) with Processing.
<br>
<br>
For our assignment this week, we played with her interfaces and build our upon a code made available by her and created by Simon Greenwold.
<br>
<br>
We connected the *hello.mag.45* board to the computer. With the code, we changed the USB port number according to the one of the computer (COM4). The code basically displays a shape with three different colors of light. Depending on how you move the sensor, the lights and the shape move. We started playing in changing parameters for the color of the lights. First, we learned that in Processing, the colors are displayed in RGB mode. [This tutorial about colors from the Processing webpage](https://processing.org/tutorials/color/) is very informative. We selected different colors for the three lights: pink, purple and cyan.
<br>
<br>
{% figure caption: "*Colors defined for the lights*" %}
![]({{site.baseurl}}/fa_w16_01.jpg)
{% endfigure %}
<br>
<br>
After that, we wanted to change the shape. We discovered that there are only two primitive shapes available on Processing: box and sphere. It is also possible to custom these shapes by calling “vertex()”. We learned these basic principles after reading [this tutorial called P3D on the Processing webpage](https://processing.org/tutorials/p3d/). Then we checked out other kinds of shapes, not necessarily 3D ones. [PShapes](https://processing.org/tutorials/pshape/) are the primitive shapes and we chose to use in our code the triangle. Further information about shapes can be seen in this [link](https://processing.org/examples/shapeprimitives.html).
<br>
<br>
{% figure caption: "*Defined shape*" %}
![]({{site.baseurl}}/fa_w16_02.jpg)
{% endfigure %}
<br>
<br>
The final outcome can be seen below.
<br>
<br>
{% figure caption: "*Final interface*" %}
![]({{site.baseurl}}/fa_w16_03.gif)
{% endfigure %}
<br>
<br>
**Download the file**
<br>
<br>
[w16_interface-hal_sensor_processing.pde]({{site.baseurl}}/w16_interface-hal_sensor_processing.pde)

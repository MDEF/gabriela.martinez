---
title: 13 • Applications and Implications
period: 10-17 April 2019
date: 2019-04-17 12:00:00
term: 2
published: true
---


<br>
<br>
For this week, we are asked to talk about our projects. Fab Academy project for MDEF students is actually our master’s project, so below I detailed the project I have been developing.
<br>
<br>
**Define the scope of your project**
<br>
<br>
In the hyperconnected world we live in, multiple and rich connections arise, but there is also a great tendency of losing connection to our real environments. Nobody really belongs to anywhere anymore, we are under constant global influence, and so our relationship to things. We buy goods made in faraway lands, without knowledge of material’s origins and the labor’s conditions in which they were manufactured. Moreover, the things we are buying have less and less relation to our context and cultural expressions. How to reconnect people to the trajectory of the making of a good? How to give consciousness of its individual impact or to the scale of linked impacts? Even if they have consciousness, how to engage people in making a real change on their consumption habits to choose wisely and go towards a circular chain?
<br>
<br>
With these considerations, the area of interest of this project lays in the field of making and creating bonds between people and nature in this process. As a way to explore possibilities of establishing these connections between people, place and materiality, I defined to work on a research of local connection through making in the context of micro manufacturing in the city.
<br>
<br>
The intent is to make visible that design is not only an image or final product, but it is a process, it has expertise, cultural history embedded and it is materiality. It looks to explore how we can collectively understand design and its role in our urban lives. I believe that one path to achieve this comprehension is by local connection through making. With people through identities, shared knowledge, patterns, shapes. With nature through materials, circularity and technology. By that, the project intends to connect the commons knowledge of making - craftsmanship - with the knowledge being developed in makerspaces - digital fabrication.
<br>
<br>
**What will I do?**
<br>
<br>
The area of interest lays in the field of making and creating bonds between people and nature in this process. As a way to explore possibilities of establishing these connections between people, place and materiality, I defined to work on a research of local connection through making in the context of micro manufacturing in the city. As explained before, the idea is to bridge the commons knowledge of making - craftsmanship - with the knowledge being developed in makerspaces - digital fabrication. The project is divided in three phases: *Understanding*, *Making* and *Sharing*.
<br>
<br>
{% figure caption: "*Traditional and digital craftsmanship*" %}
![]({{site.baseurl}}/fa_w13_01.jpg)
{% endfigure %}
<br>
<br>
**Develop a project plan**
<br>
<br>
With this project, I first decided to explore a craftsmanship and a technology of makerspaces that would be both new to me, so I would be able to do an exploration in first-person perspective that would bring me new perspectives. I chose to explore ceramics, both in the traditional context and with new technologies. My first design action was to attend ceramic classes to learn basic techniques - I learned the principles of coil construction, slab construction and wheel throwing. I also had conversations with designers and ceramists to understand about the context of their work. After experiencing the traditional knowledge of pottery making, I decided to see how this craft could be done in the context of a makerspace. I wanted to experience how extruding clay with a machine would be and understand what feelings or experiences could be missed or learned in this process.But first, I tried to extrude clay by hand to see the role of the machine. I realized that is really hard to control the flow of clay by hand, to keep a consistency in the extrusion of the material. With the machine, you have a compressor or a motor over a syringe that is pushing the material out and making it go out evenly. After this experiment, I moved to experience the process of extruding with a machine. I had the help of Eduardo Chamorro, tutor in the Fab Academy and Barbara Drozdek, a colleague from the master. The process of using it, in the case a small robot, to extrude clay is very time demanding on the first steps. To prepare the file, to define the settings and fill the extruder with clay was a process that took more than an hour to happen. But when it comes to extruding, it is really effective, according to the file to be printed evidently, but a simple cup took around five minutes to be done. With the machine already set, many files can later be printed in a short period of time. The potential of micro manufacturing in cities can be an amazing revolution for shifting from a model based on industry production, centralized, with a lot of transportation logistics to be done. Also, enables people’s empowerment in reflecting over the underlying politics of objects - Why this shape? Why this size? Why these colors or decorations? What all these characteristics tell me? It is also a process embedded with cultural and material context, very much related to a locality - the exploration of this project. There are now many makerspaces and Fab Labs around the world. It is an incredible network but most of them have the same standardized machines and people are using them with the same standard materials. How can making look for local resources and knowledge? What techniques a community can bring to this context? Which different materials can be found to open new possibilities of making? It is in this realm that the project is being developed.
<br>
<br>
**What has done beforehand?**
<br>
<br>
Atelier NL - Earth Alchemy Factory: The Eindhoven-based studio is researching natural raw materials from different regions on The Netherlands and also from around the world. With the project “Earth Alchemy Factory” they invite people to learn about these materials through making. They developed workshops, lectures, archives and material libraries to bring design, education and production into one system. In their own words, “we believe that understanding natural material transformation encourages more sustainable cultural evolution”.
<br>
[http://www.ateliernl.com/](http://www.ateliernl.com/)
<br>
<br>
Superlocal is a project that intends to create a global network for local manufacturing. Through design actions of local production tours and lectures, Andrea de Chirico, its creator, wants to foster a future vision on crafts and production. He started from the Fab Lab network, adding a new layer, which looks into traditional manufacturing techniques. The intention is to generate global impact by creating a network of people that connects resources and labour locally, helping their local economies, craft traditions and encouraging new manufacturing mentalities.
<br>
[https://www.super-local.org/](https://www.super-local.org/)
<br>
<br>
The Dutch studio Super-local, currently based in Africa, develops their work around design products, services and systems to improve the quality of the lives of the poor. They immerse themselves in a local unprivileged reality to help people identify their skills and knowledge, developing a hands-on approach and in collaboration with local communities.
<br>
[http://www.super-local.com/](http://www.super-local.com/)
<br>
<br>
“A factory as it might be” was an architectural installation at A/D/O by Assemble, Granby Workshop, Will Shannon & collaborators, which consisted in a set up of a model factory, equipped with clay and an industrial extruder. The idea was to explore the concept of an itinerant production, by transferring the experimental production approach of Granby Workshop, located in Liverpool to New York, where the installation occurred.
<br>
[https://assemblestudio.co.uk/projects/a-factory-as-it-might-be](http://www.super-local.com/)
<br>
<br>
**What materials and components will be required? Where they come from? What it is the cost?**
<br>
<br>
For this exploration, I have been using the tools related to traditional pottery making and others related to new technologies. The list of materials so far consists:
<br>
• Clay - made in Esparreguera, Spain. It is 45 km away from Barcelona. Around 6 euros for 12kg.
<br>
• Pottery modeling kit with 8 tools. Made in China. Around 7 euros, bought in a brick and mortar shop in Barcelona.
<br>
• ABB robotic arm - available in Fab Lab Barcelona.
<br>
• Air compressor - available in Fab Lab Barcelona.
<br>
• Set of pieces for adapting the robot to extrude clay - available in Fab Lab Barcelona.
<br>
• Water - to mix with the clay and make it easier to be extruded.
<br>
• Electricity  - to be able to use the machines.
<br>
<br>
Besides the price of basic tools and materials already specified, the price for ceramic 3D printing it is hard to define since the work it is being done in an academic environment. From talking with service providers of ceramic 3D printing, one of them has the value of 21 euros per hour of printing and another one is 7 euros per hour. It becomes hard to estimate with such difference in values, since each of the makers has somehow it is own cost of infrastructure, energy, machinery and materials. By considering the cheaper service provider, the print of the 3 quarters of plates would cost 108,10 euros - this would include printing service and materials.
<br>
<br>
**What parts and systems will be made? What processes used?**
<br>
<br>
The project is based more in a theoretical and experiential approach, with hands on activities but not machine building or electronic design. The results produced will be directly linked to the experimentation of techniques - of traditional and digital craftsmanship. The processes used are, from traditional craftsmanship: coil construction, slab construction and wheel throwing; from digital craftsmanship: ceramic 3D printing.
<br>
<br>
**What tasks need to be completed?**
<br>
<br>
As I am undergoing the *Making* phase, it is still work in progress. Additionally, the *Sharing* part needs to be done, in which my exploration will be shared with the ceramic community and they will be invited to develop a work in their cities to bridge traditional craftsmanship with new technologies.
<br>
<br>
**What questions need to be answered?**
<br>
<br>
A lot of questions have arisen in the making phase due to limitations of fabrication. It is needed to evaluate when bringing new technologies are a viable process for production in the context described, of micro manufacturing in the city.
<br>
<br>
**What is the schedule?**
<br>
<br>
22/04 to 26/04 - identifying and contacting possible partners
<br>
29/04 to 03/05 - visiting partners and defining collaborations
<br>
06/05 to 10/05 - structuring methodologies
<br>
13/05 to 17/05 - prototyping ideas
<br>
20/05 to 24/05 - prototyping ideas
<br>
27/05 to 31/05 - deploying ideas
<br>
03/06 to 07/06 - deploying ideas + working on the presentation
<br>
<br>
**How will it be evaluated?**
<br>
<br>
As the project starts with a first-person perspective exploration, my personal impressions will be made available and the idea is that makerspaces around the world start to connect with commons knowledge of their surroundings. A descriptive process to start this local connection will be provided to them to be evaluated.

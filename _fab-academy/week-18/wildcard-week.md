---
title: 18 • Wildcard Week
period: 22-27 May 2019
date: 2019-05-27 12:00:00
term: 3
published: true
---


<br>
<br>
Assignment: design and produce something with a digital fabrication process (incorporating computer-aided design and manufacturing) not covered in another assignment
<br>
<br>
For the *wildcard* week, I decided to explore robotics in manufacturing. As already explained in the [Applications and Implications week](https://mdef.gitlab.io/gabriela.martinez/fab-academy/applications-implications/), my final project intends to connect the commons knowledge of making - craftsmanship - with the knowledge being developed in makerspaces - digital fabrication. For that, I looked into the field of pottery manufacturing to learn from the traditional techniques to the ones being developed with new technologies. Ceramic 3D printing is an emerging and very collaborative field, with people developing tools and machines for the extrusion of clay and sharing their knowledge. Wikifactory has recently establish a [forum community](https://wikifactory.com/+Ceramic3DPrinting) about this topic with the help of pioneers in the field, like [Unfold](http://unfold.be/). Ceramic 3D printing can be done with machines like 3D printers or with robotic arms adapted with extruders. Since IAAC/Fab Lab Barcelona do not have 3D printers adapted to ceramic printing, we were going to use one of the many robotic arms they have.
<br>
<br>
The assignment started with a introductory class of robotics with Fab Academy tutor Eduardo Chamorro. He also would be assisting on the hands-on activity later. In the class, Chamorro introduced us to the current use of these tools, mostly in the industrial setting of automobile production and high complexity parts production. The robots are 6 axis manipulator/external axis tools, with no predefined process. It is highly customizable with the addition of end-tools, and for printing with clay, we would have a cartridge for containing clay and a nozzle to extrude. Also, a air compressor is needed to push the clay out through the nozzle. With the possibility of adapting the robot with many tools, its applicability is varied, it can measure, 3D scan, cut, mill, draw, spray, 3d print, assemble and many more.  
<br>
<br>
In the class, Chamorro presented to us concepts related to the production with robots, its logics as *inverse and forward kinematics*, and also the constraints, named *singularities*. The whole system is comprised of, besides the robot, a power house, kinetics control and a computer control + pendant control. The robot has 3 coordinates systems: base (floor) + tool + base object. And it rotate each motor individually. After this more "physical" introduction in the world of robotics, we learned how to set a file to be printed. Chamorro was guiding us in the development of a file in Rhinoceros, with Grasshopper and the plugin Taco ABB that enables the simulation of the job the robot will execute.Our first file was to make the robot follow a line, and we had to place the line in the tip of the nozzle.
<br>
<br>
{% figure caption: "*First file to make the robot follow a line*" %}
![]({{site.baseurl}}/fa_w18_01.jpg)
{% endfigure %}
<br>
<br>
{% figure caption: "*Visualization of the robot and the line*" %}
![]({{site.baseurl}}/fa_w18_02.jpg)
{% endfigure %}
<br>
<br>
Next, we headed to the lab to experiment with the robot. But before, we need to prepare the clay and fill the cartridges with it. To prepare the clay means to mix it with water, to make it more fluid and easier to be extruded. We were using a local grogged red earthenware clay. We first took the clay out of its package and broke it in small parts, added to a bucket and added water. It is good to mix it with hand to break the big chunks of clay and let it mix evenly with water. The amount of water added was done by eye and perception of the viscosity of the clay. So, we were adding water little by little to reach a good texture to be extruded. After this process, we will have the material ready to be added to a cartridge.
<br>
<br>
{% figure caption: "*Label of the clay used*" %}
![]({{site.baseurl}}/fa_w18_03.jpg)
{% endfigure %}
{% figure caption: "*Process of mixing with water*" %}
![]({{site.baseurl}}/fa_w18_04.jpg)
{% endfigure %}
<br>
<br>
The process of adding clay to a cartridge can be tricky. Barbara Drozdek has a technique very effective that minimize the presence of bubbles inside the cartridge. If there are bubbles inside the clay, they will explode when extruded by the nozzle, destroying the printing. We had some trouble with some bubbles in our first attempt of printing and also the clay was not that much fluid, so we needed to add more water and mix again.
<br>
<br>
<iframe width="672" height="378" src="https://www.youtube.com/embed/t0s-RQfMxF4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<br>
<br>
To start printing, first it is needed to calibrate the end tool of the robot and set the zero coordinates for X, Y and Z. In the lab, this is done with the help of a pointer, as seen in the image below.
<br>
<br>
{% figure caption: "*Calibrating the robot*" %}
![]({{site.baseurl}}/fa_w18_05.jpg)
{% endfigure %}
<br>
<br>
After that, we can load the file - a gcode - and first analyse its trajectory to see if it is doing what we planned. If it is all good, we can run the file again and this time open the valve of the air compressor to allow clay to be released. We did some first tests before trying to print our designs. Actually, since this is part of my final project, I have been going back to the robot a few times, with the help of Eduardo Chamorro. The final video here is of a successful printing, the making of a little plate.
<br>
<br>
<iframe width="672" height="378" src="https://www.youtube.com/embed/H1imIFEnVAA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<br>
<br>
<iframe width="672" height="378" src="https://www.youtube.com/embed/FoHo1eUQ9VM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<br>
<br>
**Download the files**
<br>
<br>
[fa_w18_robot_test.gh - grasshopper file](fa_w18_robot_test.gh)
<br>
[fa_w18_3dRoboticPrintingClaySample.gh - grasshopper file](fa_w18_3dRoboticPrintingClaySample.gh)

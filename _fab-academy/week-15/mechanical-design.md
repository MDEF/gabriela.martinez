---
title: 15 • Mechanical Design
period: 30 April - 08 May 2019
date: 2019-05-08 12:00:00
term: 3
published: true
---


<br>
<br>
The *Mechanical Design* week and *Machine* week of Fab Academy were contemplated by MDEF students during the seminar **From Bits to Atoms** done in the first term of the master. More information can be found here:
<br>
<br>
[https://mdef.gitlab.io/dj-pom-pom/](https://mdef.gitlab.io/dj-pom-pom/)
<br>
<br>
And my personal reflections, can be found here:
<br>
<br>
[https://mdef.gitlab.io/gabriela.martinez/term-1/from-bits-to-atoms/](https://mdef.gitlab.io/gabriela.martinez/term-1/from-bits-to-atoms/)
<br>
<br>
{% figure caption: "*DJ Pom Pom, a machine to make pom poms*" %}
![]({{site.baseurl}}/w11_01.jpg)
{% endfigure %}

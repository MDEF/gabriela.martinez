---
title: 14 • Networking and Communications
period: 24-30 April 2019
date: 2019-04-30 12:00:00
term: 3
published: true
---


<br>
<br>
Assignment: read a sensor value and send the data to your team mate
<br>
<br>
For the [class with local tutors](http://fabacademy.org/2019/labs/barcelona/local/wa/week13/), the concepts around the *Networking and Communications* week were explained more in dept. It started with explanations about two different kinds of communications: Asynchronous and Synchronous. The Asynchronous happens when data is transferred without timing, and these are RX/TX and V-USB. The Synchronous, you have timing with the data, so you know the timing between each byte, and these are 12C and SPI. Both can be wired communication or wireless - depends on how secure you want to be. In the context of wired communication, there are communication via hardware, which are parallel interface and another way is by serial interface, which is more simple. Besides these four examples of communication mentioned below, in the class we were introduced to UART, OTA communication, HC08 communication (Bluetooth), Wi-Fi, NRF2 communication and HC12 communication.
<br>
<br>
For doing the assignment with Barbara Drozdek, we first looked out for examples to have inspiration. We wanted to try out something effective and simple, so we gathered some projects available on the internet:
<br>
<br>
• [Two leds communication with Arduino](https://www.instructables.com/id/Arduino-Communication/)
<br>
• [Led and potentiometer with Arduino](https://www.instructables.com/id/Communicating-With-Two-Arduinos/)
<br>
• [Arduino Serial Communication | Send or Recieve Message from PC to Arduino](https://www.youtube.com/watch?v=km9XDErG2PY)
<br>
• [Morse code communication](https://www.instructables.com/id/Communication-Using-Morse-Code/)
<br>
• [Button and led communication with Arduino](https://www.instructables.com/id/Communication-Between-Two-Arduinos-I2C/)
<br>
<br>
We decided to do a I2C communication, the last tutorial on the list above. It works with 2 Arduinos connected, one that would have a button and be the master and the other one with a led, would be the slave.
<br>
<br>
We first connected all the cables according to image below.
<br>
<br>
{% figure caption: "*scheme for I2C communication*" %}
![]({{site.baseurl}}/fa_w14_01.jpg)
{% endfigure %}
<br>
<br>
Then, we uploaded the codes, a master code for the Arduino with the button and a slave code for the Arduino with the led. Before uploading, we unplugged the wires connecting both Arduinos, as a measure of safety.
<br>
<br>
{% figure caption: "*the master Arduino*" %}
![]({{site.baseurl}}/fa_w14_02.jpg)
{% endfigure %}
{% figure caption: "*the slave Arduino*" %}
![]({{site.baseurl}}/fa_w14_03.jpg)
{% endfigure %}
{% figure caption: "*Arduinos connected*" %}
![]({{site.baseurl}}/fa_w14_04.jpg)
{% endfigure %}
<br>
<br>
With the codes uploaded, we replugged the wires in between the Arduinos and added two more: connecting 5V of one arduino to 5V of the other, and the same procedure with ground connection. We did this as instruction from our Fab Academy tutor so we could power the scheme from one of our computers. We powered it and tried the button, but the led did not work out. We changed the led, thinking that it would be burned. It still did not work. So we started to revise the connections and check the code. We noticed that on the code, the led port was different from the one we connected, so we changed that and it worked.
<br>
<br>
{% figure caption: "*First try*" %}
![]({{site.baseurl}}/fa_w14_05.jpg)
{% endfigure %}
<br>
<br>
<iframe width="672" height="378" src="https://www.youtube.com/embed/LM8FZ3Fabpk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
<br>
<br>
**Download the files**
<br>
<br>
[w14_arduino-slave.ino](w14_arduino-slave.ino)
<br>
[w14_arduino-master.ino](w14_arduino-master.ino)

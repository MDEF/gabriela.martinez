---
title: 11 • Input Devices
period: 27 March - 03 April 2019
date: 2019-04-03 12:00:00
term: 2
published: true
---


<br>
<br>
Assignment: Measure something. Replicate a Neil’s board. - work in pairs
<br>
<br>
For this week assignment I worked with Barbara Drozdek. We wanted to start working in parts for making a pottery wheel in the week machine, but this project was postponed. Anyways, as a way to test inputs and outputs related to a project of a pottery wheel, we chose to make the board with the hal sensor for measuring magnetic field. This would be useful to measure the rotations in order to create a closed loop system with a DC Motor.
<br>
<br>
From this [week’s class page](http://academy.cba.mit.edu/classes/input_devices/index.html) we could get all the information for making the *hello.mag.45* board, the one that has the hal sensor to measure magnetic fields. We downloaded the images for the traces and the outcut line, processed it on fabmodules with the following parameters:
<br>
<br>
{% figure caption: "*Fabmodules - traces parameters*" %}
![]({{site.baseurl}}/fa_w11_01.jpg)
{% endfigure %}
{% figure caption: "*Fabmodules - outcut parameters*" %}
![]({{site.baseurl}}/fa_w11_02.jpg)
{% endfigure %}
{% figure caption: "*Milling process*" %}
![]({{site.baseurl}}/fa_w11_03.jpg)
{% endfigure %}
<br>
<br>
After cutting the board, it was time to solder the components. For the  *hello.mag.45* board it is needed:
<br>
-(01) IC A1324
<br>
-(01) IC Attiny45
<br>
-(01) Resistor 10k
<br>
-(01) Capacitor 1uF
<br>
-(01) Connector 2x3
<br>
-(01) Connector 1x6
<br>
<br>
{% figure caption: "*Soldering process and final board*" %}
![]({{site.baseurl}}/fa_w11_04.jpg)
{% endfigure %}
<br>
<br>
With the board done, I checked the voltage flow in the board with multimeter, everything was fine.
<br>
<br>
{% figure caption: "*Checking with multimeter*" %}
![]({{site.baseurl}}/fa_w11_05.jpg)
{% endfigure %}
<br>
<br>
Then, for programming the board, I connected it to the FabISP done previously, with attention to the way both connect - with correct vcc and ground orientation. For programming, I followed the initial steps from Nicolo Gnecchi. Available here:
<br>
[http://fab.academany.org/2018/labs/barcelona/students/nicolo-gnecchi/input-devices/](http://fab.academany.org/2018/labs/barcelona/students/nicolo-gnecchi/input-devices/)
<br>
<br>
{% figure caption: "*Connecting FabISP and board*" %}
![]({{site.baseurl}}/fa_w11_06.jpg)
{% endfigure %}
<br>
<br>
The process was:
<br>
<br>
Open terminal and type:
<br>
make -f hello.mag.45.make
<br>
<br>
This will compile the code and generate 2 new archives: .hex and .out
<br>
<br>
After, type:
<br>
make -f hello.mag.45.make program-usbtiny
<br>
<br>
At this point, we got an error with usbtiny connection, not being able to program the board. This process is described in the file “programming_01.text” available at the end of the documentation.
<br>
<br>
We tried to program with the AVR mk2 from the lab, but it also did not worked. We moved on to check problems on the board. Then, we resoldered some components two times. After the second attempt to correct any physical problem, things sorted out.
<br>
<br>
The board was successfully programmed. It is possible to check the process in the file “programming_02.txt”.
<br>
<br>
To test that our board was functioning, we used the files created by Carolina Vignoli for her Interface and Application programming. It is available here:
<br>
[http://archive.fabacademy.org/fabacademy2016/fablabbcn2016/students/346/Week16.htm](http://archive.fabacademy.org/fabacademy2016/fablabbcn2016/students/346/Week16.htm)
<br>
<br>
Her projects were based on code developed by Tiago Figueiredo. These are available here:
<br>
<br>
[http://fabacademy.org/archives/2014/students/figueiredo.tiago/assignments/week15.html](http://archive.fabacademy.org/fabacademy2016/fablabbcn2016/students/346/Week16.htm)
<br>
<br>
We opened Carolina’s file in Processing, changed the board number according to the one of the computer (COM4) and tested the reading of the sensor. We had a magnet to play with it. We comproved that the board was functioning correctly.
<br>
<br>
{% figure caption: "*Testing the board with a magnet*" %}
![]({{site.baseurl}}/fa_w11_07.gif)
{% endfigure %}
<br>
<br>
**Download the files**
<br>
<br>
[hello.mag.45.traces.png]({{site.baseurl}}/hello.mag.45.traces.png)
<br>
[hello.mag.45.interior.png]({{site.baseurl}}/hello.mag.45.interior.png)
<br>
[hello.mag.45.make]({{site.baseurl}}/hello.mag.45.make)
<br>
[programming_01.txt]({{site.baseurl}}/programming_01.txt)
<br>
[programming_02.txt]({{site.baseurl}}/programming_02.txt)

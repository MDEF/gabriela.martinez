---
title: 09 • Embedded Programming
period: 13-20 March 2019
date: 2019-03-20 12:00:00
term: 2
published: true
---


<br>
<br>
Assignment: program your board to do something
<br>
<br>
*Microcontroller datasheet*
<br>
The microcontroller used for the echo hello-world board was an ATtiny44. Here you can find its datasheet with further information:
<br>
<br>
[Microcontroller with 2K/4K/8K Bytes In-System Programmable Flash](http://academy.cba.mit.edu/classes/embedded_programming/doc8183.pdf)
<br>
<br>
From it, I learned that the position of the components matter and this was a step considered during its design in the eletronics design week.
<br>
<br>
For programming the echo hello-world board, I followed this tutorial:
<br>
<br>
[http://highlowtech.org/?p=1695](http://highlowtech.org/?p=1695)
<br>
<br>
Basically, the steps done were:
<br>
<br>
• Installing ATtiny support in Arduino
<br>
• Connecting the board to the computer
<br>
• Programming the echo hello-world board
<br>
<br>
Below, I explain in details each step followed from the tutorial.
<br>
<br>
*Installing ATtiny support in Arduino*
<br>
<br>
I added the ATtiny support URL to the “Additional Boards Manager URLs” from the Preferences of the Arduino software. Then I installed the board package at the “Boards Manager” in the Tools menu - Board.
<br>
<br>
*Connecting the board to the computer*
<br>
<br>
First, I needed to do a cable to connect the FabISP board and the echo hello-world board. With cables and two clips, I created the cable to connect the boards through their 6-pin connector.
<br>
<br>
{% figure caption: "*Pieces to make a cable to connect the boards*" %}
![]({{site.baseurl}}/fa_w09_01.jpg)
{% endfigure %}
{% figure caption: "*FabISP and echo hello-world board connected*" %}
![]({{site.baseurl}}/fa_w09_02.jpg)
{% endfigure %}
<br>
<br>
Then, at the Arduino software, in the Tools menu I selected:
<br>
Board: “ATtiny24/44/84”
<br>
Processor: “ATtiny44”
<br>
Clock: “External 20 MHz”
<br>
Programmer: “USBtinyISP”
<br>
Later, I run the “Burn Bootloader” to apply the changes.
<br>
<br>
*Programming the echo hello-world board*
<br>
<br>
Following the tutorial, I selected a sketch from the example’s library of Arduino, the "Blink" file. To adjust the code for the echo hello-world board, I needed to change the pin number, accordingly to the “translation” of the Arduino pinout info to the ATtiny pinout info.
<br>
<br>
{% figure caption: "*Pinout correspondence from Arduino to ATtiny*" %}
![]({{site.baseurl}}/fa_w09_03.jpg)
{% endfigure %}
<br>
<br>
For my echo hello-world board, the pin for the led corresponded to the number 2. After adjusting the code, I compiled it and uploaded. It worked out and the led blinked.
<br>
<br>
{% figure caption: "*Led blinking*" %}
![]({{site.baseurl}}/fa_w09_04.jpg)
{% endfigure %}
<br>
<br>
Later, I tested a code to see if the button would turn on the led. For that, a simple code was used, available at Fab Lab Github page:
<br>
<br>
[https://github.com/fablabbcn/arduino-workshops/blob/master/Hello%2044/blink_hello44/blink_hello44.ino](https://github.com/fablabbcn/arduino-workshops/blob/master/Hello%2044/blink_hello44/blink_hello44.ino)
<br>
<br>
Again, I checked the pinout numbers, adjusted it on the code to make it work. The button successfully turned on the led.
<br>
<br>
{% figure caption: "*Button turn on the led*" %}
![]({{site.baseurl}}/fa_w09_05.jpg)
{% endfigure %}
<br>
<br>
**Download the files**
<br>
<br>
[w09_embedded_programming_blink - ino file]({{site.baseurl}}/w09_embedded_programming_blink.ino)
<br>
[w09_embedded_programming_blink-button - ino file]({{site.baseurl}}/w09_embedded_programming_blink-button.ino)

---
title: 19 • Invention, Intellectual Property and Income
period: 29 May - 05 June 2019
date: 2019-06-05 12:00:00
term: 3
published: true
---


<br>
<br>
Assignment: develop a plan for dissemination of your final project
<br>
<br>
As I am a student from the Master in Design for Emergent Futures, I see that the definitions for the assignment of this week are tied to some information in accordance to Institute of Advanced Architecture of Catalonia (IAAC) definitions.
<br>
<br>
From IAAC’s Terms and Conditions 2018-2019 that we signed in the beginning of the course, the item *Intellectual Property, Patents, Copyrights* that defines the guidelines for the master courses, highlight a special condition for MDEF students:
<br>
<br>
*”SPECIAL NOTE FOR MASTER IN DESIGN FOR EMERGENT FUTURES STUDENTS:
The copyright license regulation for the projects developed in the MDEF program inherent to
the Fab Academy program will follow the regulation of the Fab Academy Program: Default license for student's work will be under Creative Commons: Attribution-ShareAlike 4.0 International (CC BY-SA 4.0). Students interested in researching further can change their licensing if expressly mentioned.”*
<br>
<br>
So, initially our projects are defined to follow the Creative Commons license, and I believe that, considering the purpose of this master, it is a right decision to share the projects under this license. The Creative Commons allows creators to share their projects under a license without having to pay fees. There are many variants under this license, but all of them share the common definition that projects are open to be distributed and allow others to build on and share.
<br>
<br>
Specifically about the Creative Commons: Attribution-ShareAlike 4.0, more information can be found in the [page of the foundation that created the licensing system](https://creativecommons.org/licenses/by-sa/4.0/). But I will highlight some of its attributes:
<br>
• Share and Adapt - you can copy and redistribute the material in any medium or format and also remix, transform and build on, with any purpose, even commercially.
<br>
•  Attribution - It is needed to give appropriate credit, with a link to the license, and inform if it is already an altered version of a previous project or not.
<br>
•  ShareAlike — Whenever someone transforms, adapts or build upon a project, it must be distributed under the same license as the original.
<br>
<br>
The Creative Commons license was somehow a revolution in the world of licensing, since there is very little focus on how to share creative projects without going in the traditional route of patents and copyrights. These routes are also time and money demanding, since there are many bureaucratic steps to go through. With Creative Commons, a creator can instantly set the definitions of licensing for its creation and also instantly share it. With the rise of discussions around open source and open design, Creative Commons is a alley tool for these universes. A lot of discussion around these very new concepts is happening, since it is still an emergent subject. It is possible to find interesting guidelines, as for example, the [Open Making Manifesto](https://openmaking.is/).

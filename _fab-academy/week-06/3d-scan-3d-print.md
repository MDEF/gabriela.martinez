---
title: 06 • 3D Scan and 3D Print
period: 20-27 February 2019
date: 2019-02-27 12:00:00
term: 2
published: true
---


<br>
<br>
Assignment: design and 3D print an object and 3D scan an object
<br>
<br>
This week at Fab Academy we took a look at addictive processes. Neil presented us the different techniques, materials to be used, files formats and softwares to 3D model in order to 3D print. Also we saw how to 3D scan. Scanning an object is a really good technique to have a 3D model of it. There are different tools and techniques for it, and I decided to explore the Kinect scanning that our lab has.
<br>
<br>
Along with Veronica, we helped each other in the 3D scanning task, so while one could experiment the technique, the other was holding or turning the object or even being scanned (we scanned our hands). To scan with Kinect we use a software called Skanect. We tried different strategies to scan: move the kinect around the object, to have the object in a support that we rotated, always taking images in different angles.
<br>
<br>
After generating the model, in the Skanect software, I did minor adjustments of rotating and cropping unwanted parts. I scanned Veronica’s hand and a mug. It surprised me that the software did not recognize the handler of the mug, so in the end I just had a cup shape.
<br>
<br>
{% figure caption: "*3D scan process of a mug*" %}
![]({{site.baseurl}}/fa_w06_01.jpg)
{% endfigure %}

{% figure caption: "*3D scan process of Veronica’s hand*" %}
![]({{site.baseurl}}/fa_w06_02.jpg)
{% endfigure %}
<br>
<br>
The files were exported as .obj, so it is possible to be open on a 3D modelling software for further exploration.
<br>
<br>
For the task of 3D printing something you created, I wanted to keep exploring the design tools of Fusion 360. Since my knowledge with it is still too basic, I decided to make a simple two piece mold that I can use with my material developed in the class of Material Driven Design. It can also be used as a mold for concrete as well. The idea was to design a candle holder. I decided to go with basic shapes and designed it in a conic shape. The commands explored were: loft from two sketch shapes, shell, copy, scale, hole, extrude.
<br>
<br>
{% figure caption: "*3D model design process*" %}
![]({{site.baseurl}}/fa_w06_03.gif)
{% endfigure %}
<br>
<br>
To print these models, I did each one in different 3D printers. The inner part was done in a RepRap and the outside part in a Makerbot. Both pieces were printed in PLA, one in white and the other in green filament. Preparing the files for it where quite similar, just a few configurations were different according to the piece to be printed or to the printer to be used. The outside mold didn’t extrapolated the inclination of 45º, so it did not need supports to hold it. Both plates were preheated and the models had a ‘skirt’ type of adhesion to it.
<br>
<br>
{% figure caption: "*RepRap printing the inner part of the mold*" %}
![]({{site.baseurl}}/fa_w06_04.jpg)
{% endfigure %}

{% figure caption: "*MakerBot printing the outside part of the mold*" %}
![]({{site.baseurl}}/fa_w06_05.jpg)
{% endfigure %}
<br>
<br>
The result of the printing was quite good except some minor irregularities in the printing done by the MakerBot, which is a machine a bit old already in the lab. The inner part took about 2 hours and 20 minutes to be printed and the outside part about 1 hour and 40 minutes. They were quite quick because of their simplicity, I am looking forward to design more complex shapes and see the results of the 3D printing of more intricate designs.
<br>
<br>
{% figure caption: "*3D printed pieces*" %}
![]({{site.baseurl}}/fa_w06_06.jpg)
{% endfigure %}
<br>
<br>
**Download the files**
<br>
<br>
[w06_3d_printing_model - step file]({{site.baseurl}}/w06_3d_printing_model.step)
<br>
[w06_3d_printing_model_exterior - stl file]({{site.baseurl}}/w06_3d_printing_model_exterior.stl)
<br>
[w06_3d_printing_model_interior - stl file]({{site.baseurl}}/w06_3d_printing_model_interior.stl)

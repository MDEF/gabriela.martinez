---
title: 03 • Computer-aided Design
period: 30 January - 05 February 2019
date: 2019-02-05 12:00:00
term: 2
published: true
---


<br>
<br>
Assignment: model a possible final project and post it on your website
<br>
<br>
I am familiar with design softwares as I am architect and for many years worked using tools like AutoCAD for 2D drawing and Sketchup for 3D modelling. So, I wanted to explore different tools for this week, and my intent was to go for open source tools. I chose to explore Fusion 360 and also want to learn Blender, I am looking forward for Blender classes with our local tutor. Fusion 360 is a tool that do not request payment for use from specific groups like students or start-up or amateurs users. It is unclear if in a near future they will start charging for the use of the software, but for the moment, it is a very powerful tool available for free. Blender is a open source software, supported by a very passionate community.
<br>
<br>
Since I am familiar with SketchUp, another free software, I started playing with some ideas for my solar lamp there. With a shape in mind, later I moved to explore it with Fusion 360.
<br>
<br>
{% figure caption: "*interface for SketchUp and Fusion 360*" %}
![]({{site.baseurl}}/fa_w03_01.jpg)
{% endfigure %}
<br>
<br>
In SketchUp, I started designing a rectangular base, later creating the arc shapes. I extruded that shape to form the base for the solar lamp. To create the translucent part of the lamp, I used the ‘follow me’ tool to create 2 quarter parts of a sphere and a half cylindrical shape. To show the progress of the work, I created a gif image you can see below. I made all the gifs in this post with [Gif Maker](https://gifmaker.me/).
<br>
<br>
{% figure caption: "*modelling process on SketchUp*" %}
![]({{site.baseurl}}/fa_w03_02.gif)
{% endfigure %}
<br>
<br>
With the desired shape in mind, I migrated to Fusion 360 to model it there. I learn that Fusion creates different “spaces” for designing, and I mostly stayed in the sketch space and the shape space. First you create the lines and forms in the sketch environment to later use this to create your shapes. Drawings are separated in layers for the sketches and the bodies.
<br>
<br>
I started making the basic sketches for the lamp base to later extrude its shape. For the translucent part of the lamp, I first created 2 spheres that later I used the tool ‘split bodies’ to break them in the shape I wanted. To make the cylindrical shape, I used the ‘pipe’ tool. After achieving creating the model for the solar lamp, I changed to the render environment and started playing with the materials to see what would best fit my model for the render.
<br>
<br>
{% figure caption: "*modelling process on Fusion 360*" %}
![]({{site.baseurl}}/fa_w03_03.gif)
{% endfigure %}
<br>
<br>
After selecting the materials I wanted, I proceeded to the render tool and selected some basic configurations for it. I tried to use the Cloud Renderer out of curiosity but a message popped out saying that my drawing would be in a queue and it would take more than 2 hours to be rendered. Then I decided to give a try to the Local Renderer and the process of rendering with it took less than 5 minutes. Later I want to explore the advanced configurations for rendering in Fusion.
<br>
<br>
{% figure caption: "*Render configurations on Fusion 360*" %}
![]({{site.baseurl}}/fa_w03_04.jpg)
{% endfigure %}
<br>
<br>
{% figure caption: "*Render image on Fusion 360*" %}
![]({{site.baseurl}}/fa_w03_05.jpg)
{% endfigure %}
<br>
<br>
*Conclusions*
<br>
<br>
From this first interaction with Fusion 360, I could notice that the software is a powerful tool from design to making; and also to visualize by rendering. It is a complete tool but I found the file extensions somehow limited and weird. The version-control system is interesting to follow up with the changes in the file. I will need more time of exploration with it to get used to its logic, which is substantially different from Autocad, a tool I am much familiar.
<br>
<br>
**Download the file**
<br>
<br>
[w03_computedr-aided_design - step file]({{site.baseurl}}/w03_computer-aided_design.step)

---
title: 10 • Molding and Casting
period: 20-27 March 2019
date: 2019-03-27 12:00:00
term: 2
published: true
---


<br>
<br>
Assignment: design a 3D mold around the stock and tooling that you'll be using, mill it, and use it to cast parts
<br>
<br>
For molding and casting week, I worked with Barbara Drozdek. Initially, we discussed the idea to cast a ring, but after doing the model and some test prints of it with a 3D printer, we realized that the shape we wanted to build would be a bit complex. We then decided to do a necklace, that we would cast with pine resin and add unused/wasted components from the electronics lab.
<br>
<br>
After having the [weekly class](http://fabacademy.org/2019/labs/barcelona/local/wa/week9/) with the local tutors, in which was presented all possible processes to do molding and casting, we wanted to experiment with milling the machinable wax for the mold. The machinable wax is resistant to high temperatures, so it can be milled in the small CNC. For that, we prepared the wax, by melting old parts and pouring it on a plywood mold. The good part of the wax is that it can be recycled and used again.
<br>
<br>
{% figure caption: "*Process of melting the wax and making a block*" %}
![]({{site.baseurl}}/fa_w10_01.jpg)
{% endfigure %}
<br>
<br>
With the wax block ready, we needed to flatten the surface to be milled. Barbara proceeded to do that on the CNC machine. A file done in RhinoCAM set parameters of pocketing as a machining operation, considering an end mill of 10mm. The wax piece was hold by wood parts fixed in the bed of the CNC machine. In the machine, the Z coordinate was defined in the middle of the wax block and X and Y coordinates a bit outside the block to make sure that everything will be flattened.
<br>
<br>
{% figure caption: "*Process of flattening the wax*" %}
![]({{site.baseurl}}/fa_w10_02.jpg)
{% endfigure %}
<br>
<br>
Initially we had already a file designed by Barbara with the necklace parts that we wanted to do, but we needed to change that after we realized that the piece of wax we had (73x170mm) was a bit smaller than that model. Also, while I was checking the file to start to mill, Santi, our tutor at Fab Lab, warned me about constraints in the design that the end mill was not going to be able to do. We had small spaces and holes that were smaller than the end mill diameter. So, initially were going to make a two-part mold and then we decided to simplify and do a one-part mold with two different shapes for necklaces. I identified all the constraints in the previous model and Barbara designed a new one on Rhinoceros. For this one-part mold we had two different shapes for necklaces, one rectangular and another one a half cylinder.
<br>
<br>
{% figure caption: "*Final design*" %}
![]({{site.baseurl}}/fa_w10_03.jpg)
{% endfigure %}
<br>
<br>
When milling the wax, for achieving precision, we first did a rough cut, with a flat end mill of 1/8" and 4 flutes. Later, we did a finishing cut, with a ball nose end mill of 1/8" and 2 flutes. The finishing cut is done to refine the surface and achieve better definition of the mold.
<br>
<br>
To proceed with the milling, we prepared the files on [Fabmodules](http://fabmodules.org/), initially for the rough cut, with the following parameters:
<br>
<br>
Input - select “mesh (.stl)” file of the design
<br>
Have the ‘view angle’ for z and x as 0 and the ‘view offset’ of y and x as 0 too.
<br>
Output - select “roland mill (.rml)”
<br>
units/in: 25.4
<br>
Dpi: 300
<br>
Transform 3D model in high map by clicking “calculate height map”.
<br>
Process - select “wax rough cut (⅛)”
<br>
Machine: SRM-20
<br>
speed (mm/s):  10
<br>
x0 (mm):   0
<br>
y0 (mm):   0
<br>
z0 (mm):   0
<br>
zjog (mm): 5
bottom intensity (0-1):  0
<br>
top intensity (0-1):  1
<br>
Direction: climb
<br>
cut depth (mm):  1
<br>
tool diameter (mm): 3.175
<br>
offset overlap (%): 50
<br>
Click to calculate the process. It will generate the toolpath in 3D dimension.
<br>
<br>
{% figure caption: "*Rough cut process*" %}
![]({{site.baseurl}}/fa_w10_04.jpg)
{% endfigure %}
<br>
<br>
For the finishing cut, the parameters were the same, only we changed the end mill and for making the file, selected “wax finish cut (⅛)” in the process tab:
<br>
<br>
{% figure caption: "*Finishing cut process*" %}
![]({{site.baseurl}}/fa_w10_05.jpg)
{% endfigure %}
<br>
<br>
The wax mold was cleaned with compressed air and was ready to be used for making the silicon mold. The next step was to prepare the materials to make it. This included the items in the photo below:
<br>
<br>
{% figure caption: "*Materials to make the mold*" %}
![]({{site.baseurl}}/fa_w10_06.jpg)
{% endfigure %}
<br>
<br>
First, the spray of mold release agent, “ease release 200” needs to be applied in the wax mold. It works as a non-stick product to make it easier to release the silicon mold after from the wax mold. After applying, the wax mold need to rest for 10 minutes. Since there were not much options left of materials, we used the silicon product Mold Max 15T, which comes in two parts to be mixed. More information can be found in the [Smooth-On webpage](https://www.smooth-on.com/products/mold-max-15t/). Before everything, to have an idea of the volume of material needed, water was poured in the wax mold and we have gotten 110mL of water as volume. With the Mold Max 15T, the mixture of part A and part B needs to be in the proportion of 10 of part A to 1 of part B. Then it was mixed very well. As our tutor Santi said, “When you know it is properly mixed, mix it again”. After that, the material was poured in the mold. To avoid the formation of air bubbles in the final mold, since the silicone is a dense viscous material, the Fab Lab vacuum pump was used for 10 minutes. After this process, it needs to rest for at least 24 hours so the silicone can get dry. After that period, the silicone mold was removed from the wax mold, and some extra silicone cutted out. The end result of the silicone mold presented some texture inside that reflected the end result of the finishing cut of the wax, which didn’t left a perfect surface. Also, the small holes done to be able to attach the necklace to a chain later, didn’t get shaped on the mold, they were to small for the silicone to penetrate. These two issues are learnings from the process to be improved next time.
<br>
<br>
{% figure caption: "*Pouring the silicone in the mold and vacuum process*" %}
![]({{site.baseurl}}/fa_w10_07.jpg)
{% endfigure %}
<br>
<br>
{% figure caption: "*silicone on the mold and final result*" %}
![]({{site.baseurl}}/fa_w10_08.jpg)
{% endfigure %}
<br>
<br>
Finally, the process of casting would start. First, we put in the mold a small component to be binded with the pine resin. The pine resin comes in small hard parts, which needs to be heated to the casted in the mold. The parts were heated in a heat plate from the lab. When it was transformed from solid to liquid, it was possible to pour it in the mold. We let it dry. The upper surface was glossy, but the one in contact with the mold was opaque and presented the texture left from the wax mold. This is an issue that needs to be improved for a more refined outcome. Also, we needed to make the small holes for holding it with a chain. By using a drill, it was a process much aggressive and the resin would break, so to make the hole for the chain, a process more delicate was needed.So Barbara came up with a solution to heat up a metal wire that would melt a hole on the resin. Then, finally we had a necklace.
<br>
<br>
{% figure caption: "*Component for the necklace*" %}
![]({{site.baseurl}}/fa_w10_09.jpg)
{% endfigure %}
<br>
<br>
{% figure caption: "*Pine resin melting process*" %}
![]({{site.baseurl}}/fa_w10_10.jpg)
{% endfigure %}
<br>
<br>
{% figure caption: "*Pine resin on the mold*" %}
![]({{site.baseurl}}/fa_w10_11.jpg)
{% endfigure %}
<br>
<br>
{% figure caption: "*Final outcome*" %}
![]({{site.baseurl}}/fa_w10_12.jpg)
{% endfigure %}
<br>
<br>
**Download the files**
<br>
<br>
[w10_molding_casting_necklaces - 3dm file](w10_molding_casting_necklaces.3dm)
<br>
[w10_molding_casting_necklaces - stl file](w10_molding_casting_necklaces.stl)

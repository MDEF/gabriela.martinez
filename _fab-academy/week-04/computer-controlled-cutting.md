---
title: 04 • Computer-controlled Cutting
period: 06-13 February 2019
date: 2019-02-13 12:00:00
term: 2
published: true
---


<br>
<br>
Assignment: cut on the vinylcutter + design and lasercut a parametric press-fit construction kit.
<br>
<br>
I find curious that cutting machines are being called as vinylcutters, since they are able to cut other materials. Then, I decided to use the machine to cut something else besides a vinyl. I wanted to explore the ability to make packaging or labels in these machines, specially glueless packaging. The way our packaging is being designed currently accounts for a considerable use of glue, synthetic glue. This makes it hard to recycle packaging, and so, for a more circular design, glueless packaging can be a good alternative to look for. [Some companies](https://beta.thedieline.com/blog/2014/6/13/glueless-oled-tv-box) are already envisioning the importance of rethinking how to pack their products.
<br>
<br>
These machines are able to cut vectorized files, so there are many softwares possible to be used for design, from Adobe Illustrator to Inkscape, with is open-source. Some cutting machines have the ability to besides cut, also perforate paper. This a feature that can give you already the places to fold pieces, which is excellent for going 3D, like making boxes. I decided to make a simple paper label for a bottle, with cutting and folding. I designed the shape and in the Silhouette software defined what would be cutted and what would be perforated (for folding).
<br>
<br>
{% figure caption: "*Label in Silhouette software*" %}
![]({{site.baseurl}}/fa_w04_01.jpg)
{% endfigure %}
<br>
<br>
{% figure caption: "*Label - process and end result*" %}
![]({{site.baseurl}}/fa_w04_02.jpg)
{% endfigure %}
<br>
<br>
For the assignment of designing a Press-fit kit, I decided to look to nature for inspiration. I searched on the website [Ask Nature](https://asknature.org) for structures that exist on nature. I enjoyed the cylindrical shape of the [moth’s case](https://asknature.org/strategy/spiral-patterned-cases-prevent-crushing/#.XGRC6jNKjIU) and thought this could be simplified in a structure that could become a shelter structure. I used Fusion 360 to design and parametrize the shapes.
<br>
<br>
{% figure caption: "*Inspiration from case seen in Ask Nature*" %}
![]({{site.baseurl}}/fa_w04_03.jpg)
{% endfigure %}
<br>
<br>
{% figure caption: "*Design process for the main piece*" %}
![]({{site.baseurl}}/fa_w04_04.gif)
{% endfigure %}
<br>
<br>
{% figure caption: "*Parametrized piece 1*" %}
![]({{site.baseurl}}/fa_w04_05.jpg)
{% endfigure %}
<br>
<br>
The *parametrized piece 1* is a piece that compounds in the hexagon done initially with the idea of creating an encased shell/shelter. Its shape comes out of the break of the hexagon in 6 pieces. It has as parametrized values the depth of the fits and its thickness, so these values can be adapted accordingly with the material used to create the kit.
<br>
<br>
{% figure caption: "*Parametrized piece 2*" %}
![]({{site.baseurl}}/fa_w04_06.jpg)
{% endfigure %}
<br>
<br>
The *parametrized piece 2* is a piece created to be able to fix the piece 1 together with others. It was designed as a hexagon to maintain a same language. It has as parametrized values the depth of the fits and its thickness, so it can also adapt to changes in the material thickness.
<br>
<br>
{% figure caption: "*Parametrized piece 3*" %}
![]({{site.baseurl}}/fa_w04_07.jpg)
{% endfigure %}
<br>
<br>
The *parametrized piece 3* was designed to be a variant of connector, since piece 2 connects pieces aligned, piece 3 would be able to connect then parallel. Its shape comes from a half hexagon and shares the same principles for its parametrized values - depth of the fits and thickness.
<br>
<br>
To cut in the laser machine of the lab, we must open the file in Rhino and prepare it as to “print” in the machine. If there are different cut parameters (engrave or cut) it is better that the file has the lines in different layers with different colors, to be defined in the software of the laser. Since I was just cutting pieces, they were in a same layer with same color. The definition of cut considers the power and speed of the laser beam. After doing some tests to find the right parameters for cutting, by sending small squares to be cutted, I sent the design file to be cutted.
<br>
<br>
{% figure caption: "*Pieces cut in the laser machine*" %}
![]({{site.baseurl}}/fa_w04_08.jpg)
{% endfigure %}
<br>
<br>
{% figure caption: "*Compositions*" %}
![]({{site.baseurl}}/fa_w04_09.gif)
{% endfigure %}
<br>
<br>
By analysing this first prototype done in the lasercutter, I believe some design improvements should be made for the future, mostly for the second and third piece. They ended up being small, which for the cardboard material felt it was not much appropriate. With bigger pieces, the idea of creating a continuous structure that can be like a shelter, altering the angles, will end in a better result.
<br>
<br>
**Download the files**
<br>
<br>
[w04_laser_cutting - step file]({{site.baseurl}}/w04_laser_cutting.step)
<br>
[w04_vinyl_cutting - dxf file]({{site.baseurl}}/w04_vinyl_cutting.dxf)
<br>
[w04_vinyl_cutting - studio3 file]({{site.baseurl}}/w04_vinyl_cutting.studio3)
